'use strict';

const faker         = require('faker');
const Mail          = require('mails').Mail;
const getUsers      = require('./users').getUsers;
const getFolders    = require('./folders').getFolders;
const randomInteger = require('lib/helpers').randomInteger;

exports.load = async function() {
  await Mail.remove({});
  let users = getUsers();

  for (let user of users) {
    let mails = getMails(user);

    for (let mail of mails) {
      await Mail.create(mail);
    }

  }

};

function getMails(user) {
  let i           = randomInteger(100, 200);
  let userFolders = getFolders(user).map(b => b._id);
  let sentId = getFolders(user).filter(f => f.name === 'sent')[0]._id;

  return Array.from({length: i}, function() {

    let currentFolder = faker.helpers.randomize(userFolders);

    let mails = {
      folder : currentFolder,
      user   : user._id,
      from   : currentFolder === sentId ? user.email : faker.internet.email(),
      to     : faker.internet.email(),
      ip     : faker.internet.ip(),
      subject: faker.lorem.words(3),
      content: faker.lorem.paragraphs(2),
      read   : currentFolder === sentId ? true : faker.random.boolean(),
      starred: faker.random.boolean(),
      date   : faker.date.between('2015', new Date()),
    };

    return mails;

  });

}

exports.getMails = getMails;
