'use strict';

const db = require('lib/db');
const config   = require('config');
const fs       = require('mz/fs');

(async() => {
  let fixtures = await fs.readdir(config.server.fixtures);

  fixtures = fixtures.map(f => {
    if (f === 'index.js') return;
    return {
      load: require(`${config.server.fixtures}/${f}`).load,
      name: f
    };
  }).filter(Boolean);

  for (let fixture of fixtures) {
    await fixture.load();
    console.log(`${fixture.name} loaded`);
  }

  db.disconnect();

})();