'use strict';

const oid  = require('lib/helpers').oid;
const User = require('users').User;

let users = ['admin', 'google'];
// 1 - 7c10223c510628f39d16c4eb
// 2 - c7a4e5d2afd2dc844a9c2f23

function getUsers() {
  return users.map(name => {
    return {
      _id     : oid(`user-${name}`),
      email   : `${name}@google.com`,
      password: `${name}`
    };
  });
}

exports.load = async function() {
  await User.remove({});
  await User.insertMany(getUsers());
};

exports.getUsers = getUsers;