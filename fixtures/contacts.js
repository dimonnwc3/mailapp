'use strict';

const Contact = require('contacts').Contact;
const faker   = require('faker');
const randomInteger = require('lib/helpers').randomInteger;
const getUsers = require('./users').getUsers;
faker.locale  = 'de';

exports.load = async function() {
  await Contact.remove({});
  let users = getUsers();

  for (let user of users) {
    await Contact.insertMany(getContacts(user));
  }

};

function getContacts(user) {
  let i = randomInteger(100, 200);
  return Array.from({length: i}, function() {
    let lastName  = faker.name.lastName();
    let firstName = faker.name.firstName();

    return {
      lastName      : lastName,
      firstName     : firstName,
      dob           : faker.date.between('1970', '2000'),
      avatarUrl     : faker.image.avatar(),
      comment       : faker.lorem.sentence(),
      addresses     : getAddresses(),
      communications: getCommunications(lastName, firstName),
      user: user._id
    };

  });
}

function getAddresses() {
  let i = randomInteger(2, 5);
  return Array.from({length: i}, function() {
    let type = faker.helpers.randomize(['Home', 'Office', 'Postal']);

    return {
      type   : type,
      comment: faker.lorem.sentence(),
      country: faker.address.county(),
      zip    : faker.address.zipCode(),
      city   : faker.address.city(),
      street : faker.address.streetName(),
      houseNo: faker.address.secondaryAddress()
    };

  });
}

function getCommunications(lastName, firstName) {
  let i = randomInteger(2, 5);
  return Array.from({length: i}, function() {

    let type     = faker.helpers.randomize(['Phone', 'Email', 'Web-Site']);
    let provider = faker.helpers.randomize(['gmail', 'yahoo', 'yandex']);
    let content;

    switch (type) {
      case 'Phone':
        content = faker.phone.phoneNumber('+49-###-###-##-###');
        break;
      case 'Email':
        content = faker.internet.email(firstName, lastName, provider);
        break;
      case 'Web-Site':
        content = faker.internet.url();
        break;
    }

    return {
      type   : type,
      comment: faker.lorem.sentence(),
      content: content
    };

  });
}

exports.getContacts = getContacts;