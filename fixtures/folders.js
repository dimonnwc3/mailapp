'use strict';

const Folder = require('folders').Folder;
const oid = require('lib/helpers').oid;
const getUsers = require('./users').getUsers;

let folders = ['inbox', 'sent', 'archive', 'trash'];

exports.load = async function() {
  await Folder.remove({});
  let users = getUsers();

  for (let user of users) {
    await Folder.insertMany(getFolders(user));
  }
};

function getFolders(user) {
  let username = user.email.slice(0, user.email.indexOf('@'));
  return folders.map(name => {
    return {
      _id   : oid(`${name}-${username}`),
      name  : name,
      user  : user._id
    };
  });
}

exports.getFolders = getFolders;
