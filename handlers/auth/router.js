'use strict';

const Router = require('koa-router');
const ctrl   = require('./controller');
let router = new Router();

router
  .post('/login', ctrl.login)
  .get('/logout', ctrl.logout);

module.exports = router.routes();