'use strict';

const mountHandler = require('lib/mountHandler');

exports.init = function() {

  return mountHandler('/api/auth', __dirname);

};