'use strict';

const passport = require('koa-passport');
const {
  EMAIL_REQUIRED_ERR,
  PASSWORD_REQUIRED_ERR
} = require('lib/constants').auth;

module.exports = {

  login: (ctx, next) => {

    if (!ctx.request.body.email) ctx.throw(400, EMAIL_REQUIRED_ERR);
    if (!ctx.request.body.password) ctx.throw(400, PASSWORD_REQUIRED_ERR);

    return passport.authenticate('local', (user, info) => {

      if (user === false) {
        return ctx.throw(401, info);
      }

      ctx.login(user);
      ctx.body = {success: true};

    })(ctx, next);

  },

  logout: ctx => {

    ctx.logout();
    ctx.body = {success: true};

  }

};
