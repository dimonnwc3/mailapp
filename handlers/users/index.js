'use strict';

const mountHandler = require('lib/mountHandler');

exports.User = require('./model');

exports.init = function() {

  return mountHandler('/api/users', __dirname);

};