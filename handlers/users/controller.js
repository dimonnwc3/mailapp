'use strict';

const Model                    = require('./model');
const CommonController         = require('lib/CommonController');
const {OLD_PASSWORD_WRONG_ERR} = require('lib/constants').user;

class UserController extends CommonController {
  constructor(Model) {
    super(Model);
    this.byReq  = this.byReq.bind(this);
    this.byId   = this.byId.bind(this);
    this.get    = this.get.bind(this);
    this.getOne = this.getOne.bind(this);
    this.post   = this.post.bind(this);
    this.patch  = this.patch.bind(this);
    this.del    = this.del.bind(this);
  }

  byReq(ctx, next) {

    ctx.params.user = ctx.user;
    return next();

  }

  async byId(id, ctx, next) {

    if (!this.isValid(id)) ctx.throw(404, `${this.Model.modelName} not found`);

    let user = await this.Model.findById(id);

    if (!user) ctx.throw(404, `${this.Model.modelName} not found`);
    let allowed = false;

    if (['GET', 'OPTIONS', 'HEAD'].find(e => e === ctx.method)) {
      allowed = true;
    }

    if (ctx.user) {
      if (String(ctx.user._id) === String(user._id) || ctx.user.isAdmin) {
        allowed = true;
      }
    }

    if (allowed) ctx.params.user = user;
    else ctx.throw(403, 'Not allowed.');
    return next();

  }

  async get(ctx) {

    let users = await Model.find({}, {}, {lean: true});
    ctx.body  = {
      entities: users.map(Model.getInfoFields),
      meta    : {}
    };

  }

  getOne(ctx) {

    ctx.body = {
      entities: [ctx.params.user.getInfoFields()],
      meta    : {}
    };

  }

  async post(ctx) {

    let user   = await this.Model.create(ctx.request.body);
    ctx.status = 201;
    ctx.body   = {
      entities: [user.getInfoFields()],
      meta    : {}
    };

  }

  async patch(ctx) {

    let user   = ctx.params.user;
    let fields = ctx.request.body;

    if (fields.password) {
      if (user.passwordHash && !user.checkPassword(fields.oldPassword)) {
        ctx.throw(400, OLD_PASSWORD_WRONG_ERR);
      }
    }

    Object.assign(user, fields);

    await user.save();
    ctx.body = {
      entities: [user.getInfoFields()],
      meta    : {}
    };

  }

  async del(ctx) {

    await ctx.params.user.remove();
    ctx.body = {
      entities: [ctx.params.user.getInfoFields()],
      meta    : {}
    };

  }

}

let ctrl = new UserController(Model);

module.exports = ctrl;