'use strict';

const Router = require('koa-router');
const ctrl   = require('./controller');
let mustBeAuthenticated = require('lib/mustBeAuthenticated');
let router = new Router();

router
  .post('/', ctrl.post)
  .use('/me', mustBeAuthenticated, ctrl.byReq)
  .get('/me', mustBeAuthenticated, ctrl.getOne)
  .patch('/me', mustBeAuthenticated, ctrl.patch)
  .delete('/me', mustBeAuthenticated, ctrl.del);

module.exports = router.routes();