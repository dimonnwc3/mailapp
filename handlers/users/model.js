'use strict';

/**
 * MODULE DEPENDENCIES.
 */

const mongoose        = require('mongoose');
const crypto          = require('crypto');
const config          = require('config');
const validator       = require('validator');
const uniqueValidator = require('mongoose-unique-validator');

const {
  EMAIL_WRONG_ERR,
  EMAIL_EMPTY_ERR,
  PASSWORD_FOUR_CHAR_ERR
} = require('lib/constants').user;
const { MONGOOSE_UNIQ } = require('lib/constants').common;

let Model;

/**
 * SCHEMA.
 */

let schema = new mongoose.Schema({
  email       : {
    type     : String,
    required : EMAIL_EMPTY_ERR,
    unique   : true,
    lowercase: true,
    trim     : true,
    validate : [validator.isEmail, EMAIL_WRONG_ERR]
  },
  passwordHash: {
    type: String
  },
  salt        : {
    type: String
  }
}, {timestamps: true});

/**
 * PLUGINS.
 */

schema.plugin(uniqueValidator, {
  message: MONGOOSE_UNIQ
});

/**
 * VIRTUALS.
 */

schema.virtual('password')
  .set(function(password) {

    if (password !== undefined) {
      if (password.length < 4) {
        this.invalidate('password',
          PASSWORD_FOUR_CHAR_ERR);
      }
    }

    this._plainPassword = password;

    if (password) {
      this.salt         = crypto.randomBytes(config.crypto.hash.length)
        .toString('base64');
      this.passwordHash = passwordHash(password, this.salt);
    } else {
      this.salt         = undefined;
      this.passwordHash = undefined;
    }

  })
  .get(function() {
    return this._plainPassword;
  });

/**
 * MIDDLEWARES.
 */

/**
 * METHODS.
 */

schema.methods.checkPassword = function(password) {
  if (!password) return false;
  if (!this.passwordHash) return false;

  return passwordHash(password, this.salt) == this.passwordHash;
};

schema.methods.getInfoFields = function() {
  return Model.getInfoFields(this);
};

/**
 * STATICS.
 */

schema.statics.getInfoFields = function(user) {
  return {
    _id        : user._id,
    email      : user.email,
    hasPassword: Boolean(user.passwordHash),
    created    : user.created
  };
};

Model = mongoose.model('User', schema);

function passwordHash(password, salt) {
  let iterations = config.crypto.hash.iterations;
  let length     = config.crypto.hash.length;
  return crypto.pbkdf2Sync(password, salt, iterations, length, 'sha512');
}

/**
 * EXPORT.
 */

module.exports = Model;