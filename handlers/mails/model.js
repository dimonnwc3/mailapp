'use strict';

const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  folder : {
    type    : mongoose.Schema.Types.ObjectId,
    ref     : 'Folder',
    required: 'Folder required'
  },
  user   : {
    type    : mongoose.Schema.Types.ObjectId,
    ref     : 'User',
    required: 'User required'
  },
  to     : {
    type    : String,
    required: 'To can\'t be empty',
    trim    : true,
  },
  from   : {
    type    : String,
    required: 'from can\'t be empty',
    trim    : true,
  },
  subject: {
    type    : String,
    required: 'Subject can\'t be empty',
    trim    : true,
  },
  content: {
    type: String,
    trim: true,
  },
  ip     : {
    type: String,
    trim: true,
  },
  read   : {
    type   : Boolean,
    default: false
  },
  starred: {
    type   : Boolean,
    default: false
  },
  date   : {
    type   : Date,
    default: Date.now
  }
}, {timestamps: true});

let Model = mongoose.model('Mail', schema);

module.exports = Model;
