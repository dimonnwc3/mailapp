'use strict';

const Model            = require('./model');
const Folder           = require('folders').Folder;
const CommonController = require('lib/CommonController');

class MailController extends CommonController {
  constructor(Model) {
    super(Model);
    this.byFolderName = this.byFolderName.bind(this);
    this.get          = this.get.bind(this);
  }

  async byFolderName(name, ctx, next) {

    const folder = await Folder.findOne({
      name: name,
      user: ctx.user
    });

    if (!folder) {
      ctx.throw(404, `Mails by '${name}' not found`);
    }

    ctx.params.folder = folder;
    return next();

  }

  async get(ctx) {

    const docsP = this.Model.find({
      folder: ctx.params.folder._id,
      user  : ctx.user
    }, {}, {lean: true});

    const countP = this.Model.count({
      folder: ctx.params.folder._id,
      user  : ctx.user
    });

    const unreadCountP = this.Model.count({
      folder: ctx.params.folder._id,
      user  : ctx.user,
      read  : false
    });

    ctx.body = {meta: {}};

    [
      ctx.body.entities,
      ctx.body.meta.count,
      ctx.body.meta.unread
    ] = await Promise.all([docsP, countP, unreadCountP]);

  }

  async post(ctx) {

    let {body: fields} = ctx.request;

    if (fields.date === '') {
      fields.date = new Date();
    }

    let doc = await this.Model.create(fields);

    ctx.status = 201;
    ctx.body   = {
      entities: [doc],
      meta    : {}
    };

  }

  async patchMany(ctx) {

    let {body: entities} = ctx.request;

    const docs = await this.Model.find({
      _id : {$in: [entities.map(e => e._id)]},
      user: ctx.user
    });

    const merged = docs.map(doc => {
      const entity = entities.find(e => e._id === doc._id);
      if (!entity) {
        return;
      }

      Object.keys(doc).forEach(key => {
        if (key === '_id') return;
        if (entity[key]) {
          doc[key] = entity[key];
        } else {
          delete doc[key];
        }
      });
    });

    await Promise.all(merged.map(d => d.save()));

    ctx.body = {
      entities: merged,
      meta    : {}
    };

  }

}

let ctrl = new MailController(Model);

module.exports = ctrl;