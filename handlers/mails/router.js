'use strict';

const Router            = require('koa-router');
const ctrl              = require('./controller');
let mustBeAuthenticated = require('lib/mustBeAuthenticated');
let router              = new Router();

router
  .use(mustBeAuthenticated)

  .param('id', ctrl.byId)
  .param('name', ctrl.byFolderName)

  .get('/:name', ctrl.get)
  .post('/', ctrl.post)
  .patch('/', ctrl.patchMany)

  .get('/:id', ctrl.getOne)
  .patch('/:id', ctrl.patch)
  .delete('/:id', ctrl.del);

module.exports = router.routes();