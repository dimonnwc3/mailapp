'use strict';

const mountHandler = require('lib/mountHandler');
exports.Mail = require('./model');

exports.init = function() {

  return mountHandler('/api/mails', __dirname);

};