'use strict';

const Model            = require('./model');
const Mail             = require('mails').Mail;
const CommonController = require('lib/CommonController');

class FolderController extends CommonController {
  constructor(Model) {
    super(Model);
    this.get = this.get.bind(this);
  }

  async get(ctx) {

    let docs = await this.Model.find({user: ctx.user}, {}, {lean: true});

    const counts = await Promise.all(
      docs.map(doc => Mail.count({folder: doc._id, user: ctx.user}))
    );

    const unread = await Promise.all(
      docs.map(doc => Mail.count({
        folder: doc._id,
        user: ctx.user,
        read: false
      }))
    );

    docs.forEach((doc, idx) => {
      doc.mails  = counts[idx];
      doc.unread = unread[idx];
    });

    ctx.body = {
      entities: docs,
      meta: {}
    };

  }

}

let ctrl = new FolderController(Model);

module.exports = ctrl;