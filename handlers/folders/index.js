'use strict';

const mountHandler = require('lib/mountHandler');
exports.Folder = require('./model');

exports.init = function() {

  return mountHandler('/api/folders', __dirname);

};