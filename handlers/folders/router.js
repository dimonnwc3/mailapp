'use strict';

const Router            = require('koa-router');
const ctrl              = require('./controller');
let mustBeAuthenticated = require('lib/mustBeAuthenticated');
let router              = new Router();

router
  .use(mustBeAuthenticated)
  .param('id', ctrl.byId)

  .get('/', ctrl.get)
  .post('/', ctrl.post)

  .get('/:id', ctrl.getOne)
  .patch('/:id', ctrl.patch)
  .delete('/:id', ctrl.del);

module.exports = router.routes();