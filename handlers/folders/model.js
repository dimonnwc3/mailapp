'use strict';

const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  user: {
    type    : mongoose.Schema.Types.ObjectId,
    ref     : 'User',
    required: 'User required'
  },
  name: {
    type    : String,
    required: 'Name can\'t be empty',
    trim    : true,
  }
}, {timestamps: true});

let Model = mongoose.model('Folder', schema);

module.exports = Model;