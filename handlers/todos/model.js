'use strict';

const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  content  : {
    type    : String,
    required: 'E-mail can\'t be empty',
    trim    : true,
  },
  completed: {
    type    : Boolean,
    required: true,
    default : false
  },
  user     : {
    type    : mongoose.Schema.Types.ObjectId,
    ref     : 'User',
    required: 'User required'
  },
}, {timestamps: true});

let Model = mongoose.model('Todo', schema);

module.exports = Model;