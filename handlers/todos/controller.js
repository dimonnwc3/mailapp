'use strict';

const Model            = require('./model');
const CommonController = require('lib/CommonController');

class TodoController extends CommonController {
  constructor(Model) {
    super(Model);
  }

  // async get(ctx) {
  //   await super.get(ctx);
  //   console.log(123);
  // }
}

let ctrl = new TodoController(Model);

module.exports = ctrl;