'use strict';

const mountHandler = require('lib/mountHandler');
exports.Todo = require('./model');

exports.init = function() {

  return mountHandler('/api/todos', __dirname);

};