'use strict';

const mountHandler    = require('lib/mountHandler');
exports.Contact       = require('./model').Contact;
exports.Address       = require('./model').Address;
exports.Communication = require('./model').Communication;

exports.init = function() {

  return mountHandler('/api/contacts', __dirname);

};