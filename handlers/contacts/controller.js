'use strict';

const Model            = require('./model').Contact;
const CommonController = require('lib/CommonController');

class ContactController extends CommonController {
  constructor(Model) {
    super(Model);
    this.get = this.get.bind(this);
  }

  async get(ctx) {

    let docs = await this.Model.find({user: ctx.user}, {
      lastName : 1,
      firstName: 1,
      user     : 1
    });

    ctx.body = {
      entities: docs,
      meta: {}
    };

  }
}

let ctrl = new ContactController(Model);

module.exports = ctrl;