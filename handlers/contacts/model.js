'use strict';

const mongoose = require('mongoose');

let addressSchema = new mongoose.Schema({
  type   : {
    type: String,
    enum: ['Home', 'Office', 'Postal']
  },
  comment: {
    type: String,
    trim: true
  },
  country: {
    type: String,
    trim: true
  },
  zip    : {
    type: Number
  },
  city   : {
    type: String,
    trim: true
  },
  street : {
    type: String,
    trim: true
  },
  houseNo: {
    type: String,
    trim: true
  }
}, {timestamps: true});

let communicationSchema = new mongoose.Schema({
  type   : {
    type: String,
    enum: ['Phone', 'Email', 'Web-Site']
  },
  comment: {
    type: String,
    trim: true
  },
  content: {
    type: String,
    trim: true
  }
}, {timestamps: true});

let contactSchema = new mongoose.Schema({
  user          : {
    type    : mongoose.Schema.Types.ObjectId,
    ref     : 'User',
    required: 'User required'
  },
  lastName      : {
    type: String,
    trim: true
  },
  firstName     : {
    type: String,
    trim: true
  },
  dob           : {
    type: Date
  },
  avatarUrl     : {
    type: String,
    trim: true
  },
  comment       : {
    type: String,
    trim: true
  },
  addresses     : [addressSchema],
  communications: [communicationSchema]
}, {timestamps: true});

let Contact       = mongoose.model('Contact', contactSchema);
let Address       = mongoose.model('Address', addressSchema);
let Communication = mongoose.model('Communication', communicationSchema);

exports.Contact       = Contact;
exports.Address       = Address;
exports.Communication = Communication;