'use strict';
if (process.env.NODE_TRACE) require('lib/trace');

const Koa = require('koa');
const config = require('config');
const fs = require('mz/fs');
const app = new Koa();

app.keys = [config.secret];

(async() => {
  let [middlewares, handlers] = await Promise.all([
    fs.readdir(`${config.server.root}/middleware`),
    fs.readdir(`${config.server.root}/handlers`)
  ]);

  middlewares
    .filter(i => !i.startsWith('.'))
    .map(m => require(`${config.server.root}/middleware/${m}`))
    .concat(handlers.filter(i => !i.startsWith('.'))
      .map(h => require(h).init()))
    .forEach(m => app.use(m));

})();

process.on('unhandledRejection', (r, p) => {
  console.error(`Possibly Unhandled Rejection at: Promise ${p} reason: ${r}`);
  console.dir(p, {colors: true});
});

module.exports = app;