'use strict';

const convert       = require('koa-convert');
const session       = require('koa-generic-session');
const db            = require('lib/db');
const MongooseStore = require('koa-session-mongoose');

const config = require('config');

const store = {
  store: new MongooseStore({
    connection: db,
    collection: 'sessions',
    expires   : config.session.cookie.maxAge / 1000 // sec
  })
};

Object.assign(config.session, store);

module.exports = convert(session(config.session));
