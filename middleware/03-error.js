'use strict';

module.exports = async (ctx, next) => {

  try {
    await next();
    if (404 == ctx.response.status && !ctx.response.body) ctx.throw(404);
  } catch (err) {

    ctx.set('X-Content-Type-Options', 'nosniff');
    ctx.type = 'application/json';
    ctx.status = err.status || 500;
    ctx.body = {success: false};
    ctx.app.emit('error', err, ctx);

    if (err.name === 'ValidationError') {
      ctx.status = 409;
      let errors = Object.keys(err.errors)
        .map(e => err.errors[e].message)
        .filter(e => !e.includes('with user: "'));
      return ctx.body.errors = errors;
    }

    ctx.body.errors = [err.message];

  }

};
