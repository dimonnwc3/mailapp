'use strict';

const passport      = require('koa-passport');
const localStrategy = require('lib/authStrategy').localStrategy;
const User          = require('users').User;

// При авторизации пишет ID юзера в this.session.passport.user
passport.serializeUser((user, done) => {
  done(null, user._id);
});

// Какждый раз по id юзера из сессии пишет юзера в req.user
passport.deserializeUser((id, done) => {
  User.findById(id, done);
});

passport.use(localStrategy);

let passportInitialize = passport.initialize();

module.exports = (ctx, next) => {
  Object.defineProperty(ctx, 'user', {
    get() {
      return ctx.req.user;
    }
  });

  return passportInitialize(ctx, next);

};
