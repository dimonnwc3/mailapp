'use strict';

module.exports = (ctx, next) => {

  if (['POST', 'PATCH', 'PUT'].includes(ctx.method) && ctx.user) {
    ctx.request.body.user = ctx.user._id;
  }

  return next();

};