'use strict';

const webpack = require('webpack');
const server = require('webpack-dev-server'); /*eslint no-unused-vars:0*/
const config = require('config');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';
const NODE_ENV = process.env.NODE_ENV || 'development';

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const precss = require('precss');

module.exports = {

  context: config.client.root,

  entry: {
    main: './index.js'
  },

  output: {
    path: config.server.public,
    publicPath: '/',
    filename: '[name].js'
  },

  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js']
  },

  resolveLoaders: {
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extensions: ['', '.js']
  },

  watch: isDev,

  watchOptions: {
    aggregateTimeout: 100
  },

  devtool: isDev ? 'cheap-inline-module-source-map' : null,

  module: {
    loaders: [{
      test: /\.js?$/,
      include: config.client.root,
      loaders: ['babel']
    },{
      test: /\.css$/,
      loader: ExtractTextPlugin
        .extract('style', 'css!postcss')
    }, {
      test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
      exclude: /\/node_modules\//,
      loader: 'url?name=[path][name].[ext]&limit=4096'
    }, {
      test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2).*$/,
      include: /\/node_modules\//,
      loader: 'file?name=[1]&regExp=node_modules/(.*)'
    }]
  },

  postcss: function () {
    return [autoprefixer, precss];
  },

  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: 'body'
    }),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: NODE_ENV
    }),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin('[name].css', {
      allChunks: true,
      disable: isDev
    })
  ],

  devServer: {
    host: config.server.host,
    port: 9000,
    contentBase: config.server.public, // если html статика
    hot: true,
    historyApiFallback: true,
    progress: true,
    inline: true,
    proxy: [{
      path: '/api',
      target: 'http://localhost:3000'
    }],
    stats: {
      colors: true
    }
  }


};

if (isProd) {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true, /*eslint camelcase:0*/
        unsafe: true
      }
    }),
    new webpack.optimize.DedupePlugin()
  );
}