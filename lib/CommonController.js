'use strict';

const ObjectId = require('mongoose').Types.ObjectId;
const isValid = ObjectId.isValid;

module.exports = class CommonController {
  constructor(Model) {
    this.Model = Model;
    this.isValid = isValid;
    this.ObjectId = ObjectId;
    this.byId = this.byId.bind(this);
    this.get = this.get.bind(this);
    this.getOne = this.getOne.bind(this);
    this.post = this.post.bind(this);
    this.patch = this.patch.bind(this);
    this.del = this.del.bind(this);
  }

  async byId(id, ctx, next) {

    if (!this.isValid(id)) ctx.throw(404, `${this.Model.modelName} not found`);

    let doc = await this.Model.findOne({user: ctx.user, _id: id});

    if (!doc) ctx.throw(404, `${this.Model.modelName} not found`);

    ctx.params.doc = doc;
    return next();

  }

  async get(ctx) {

    let docs = await this.Model.find({user: ctx.user}, {}, {lean: true});
    ctx.body = {
      entities: docs,
      meta: {}
    };

  }

  getOne(ctx) {

    ctx.body = {
      entities: [ctx.params.doc],
      meta: {}
    };

  }

  async post(ctx) {

    let {body: fields} = ctx.request;

    let doc    = await this.Model.create(fields);

    ctx.status = 201;
    ctx.body = {
      entities: [doc],
      meta: {}
    };

  }

  async patch(ctx) {

    let {doc}    = ctx.params;
    let {body: fields} = ctx.request;

    Object.assign(doc, fields);

    await doc.save();
    ctx.body = {
      entities: [doc],
      meta: {}
    };

  }

  async del(ctx) {

    await ctx.params.doc.remove();
    ctx.body = {
      entities: [ctx.params.doc],
      meta: {}
    };

  }

};