'use strict';

module.exports = function(routerModulePath) {
  var middleware = null;

  return function(ctx, next) {
    if (!middleware) {
      middleware = module.parent.require(routerModulePath);
    }
    return middleware(ctx, next);
  };

};

delete require.cache[__filename];