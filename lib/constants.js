'use strict';

module.exports = {

  user: {
    OLD_PASSWORD_WRONG_ERR: 'Old password is wrong',
    EMAIL_WRONG_ERR: 'Email is wrong',
    EMAIL_EMPTY_ERR: 'E-mail can\'t be empty',
    PASSWORD_FOUR_CHAR_ERR: 'Password should not be less than 4 character'
  },

  auth: {
    EMAIL_PASSWORD_WRONG_ERR: 'Email or password wrong',
    EMAIL_REQUIRED_ERR: 'Email is required',
    PASSWORD_REQUIRED_ERR: 'Password is required'
  },

  common: {
    MONGOOSE_UNIQ: '{PATH} "{VALUE}" exists',
    NOT_FOUND_ERR: val => `${val} not found`
  }

};