'use strict';

const LocalStrategy = require('passport-local').Strategy;
const User = require('users').User;
const { EMAIL_PASSWORD_WRONG_ERR } = require('lib/constants').auth;

let localStrategy = new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, async(email, password, done) => {

  try {
    let user = await User.findOne({email: email});

    if (!user || !user.checkPassword(password)) {
      return done(null, false, EMAIL_PASSWORD_WRONG_ERR);
    }

    done(null, user);
  } catch (err) {
    done(err);
  }

});

exports.localStrategy = localStrategy;
