'use strict';

module.exports = (ctx, next) => {
  if (ctx.isAuthenticated()) return next();
  else ctx.throw(403, 'Forbidden, authorization required');
  return next();
};
