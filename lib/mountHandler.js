'use strict';

const mount = require('koa-mount');

module.exports = function(prefix, handlerPath) {

  handlerPath += '/router';
  let handler = require('lib/lazyRouter')(handlerPath);
  if (!prefix) return mount(handler);
  return mount(prefix, handler);

};