/* VENDOR CSS */
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootswatch/darkly/bootstrap.css';
import '../node_modules/font-awesome/css/font-awesome.css';
import './dashboard.css';
import './main.css';

import './config';

import React from 'react';
import {render} from 'react-dom';
import App from './components/Common/App';

render(<App/>, document.getElementById('app'));
