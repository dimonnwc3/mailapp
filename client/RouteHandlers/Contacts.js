import React, {Component} from 'react';
import ContactList from '../components/Contacts/List';
import Header from '../components/Contacts/Header/Index';

class Contacts extends Component {
  static propTypes = {};

  render() {

    return (
      <div>
        <div className="col-sm-3 col-md-2 sidebar">
          <ContactList/>
        </div>
        <div
          className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <Header/>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Contacts;
