import React from 'react';
import FolderList from '../components/Mail/List';

class Mail extends React.Component {
  static propTypes = {};

  render() {

    return (
      <div>
        <div className="col-sm-3 col-md-2 sidebar">
          <FolderList/>
        </div>
        <div
          className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Mail;
