import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getMe} from '../AC/user';

import Header from '../components/Common/Header';
import Loader from '../components/Common/Loader';
import Notifications from '../components/Common/Notifications';

class Root extends Component {
  static propTypes = {}

  static childContextTypes = {
    user: PropTypes.object,
    isLogged: PropTypes.bool,
    location: PropTypes.object
  }

  getChildContext() {
    return {
      user: this.props.user,
      isLogged: this.props.isLogged,
      location: this.props.location
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLogged && !this.props.isLogged && !this.props.loading) {
      nextProps.getMe();
    }
  }

  componentDidMount() {
    const {isLogged, loading, loaded, getMe} = this.props;
    if (!isLogged && !loading && !loaded) {
      getMe();
    }
  }

  renderBody = () => {

    const {loading, loaded} = this.props;

    if (loading || !loaded) return <Loader/>;

    return this.props.children;

  }

  render() {

    return (
      <div>
        <Header/>
        <div className='container-fluid'>
          <div className='row'>
            <Notifications/>
            {this.renderBody()}
          </div>
        </div>
      </div>
    );

  }
}

export default connect(({user, auth}) => ({
  user: user.entities[auth.myId],
  isLogged: auth.isLogged,
  loading: auth.loading,
  loaded: auth.loaded,
  logging: auth.logging
}), {getMe})(Root);
