import React, {Component} from 'react';
import Login from '../components/Auth/Login';

class Auth extends Component {
  static propTypes = {};

  render() {

    return (
      <div>
        <div className="col-xs-12 main">
          <div className="row">
            <div className="col-xs-6"><Login/></div>
            <div className="col-xs-6"></div>
          </div>
        </div>
      </div>
    );
  }

}

export default Auth;
