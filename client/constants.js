export const START   = '_START';
export const SUCCESS = '_SUCCESS';
export const FAIL    = '_FAIL';

export const GET    = '_GET';
export const UPDATE = '_UPDATE';
export const CREATE = '_CREATE';
export const DELETE = '_DELETE';
export const ADD    = '_ADD';

export const CONTACTS = 'CONTACTS';
export const CONTACT  = 'CONTACT';
export const USER     = 'USER';
export const ME       = 'ME';
export const FILTER   = '_FILTER';
export const FOLDERS  = 'FOLDERS';
export const MAIL     = 'MAIL';

export const LOGOUT = 'LOGOUT';
export const LOGIN  = 'LOGIN';

export const SESSION_EXPIRED = 'SESSION_EXPIRED';

export const NOTIFICATION = 'NOTIFICATION';
export const MAIL_SENT_MSG = 'Your message has been sent';
