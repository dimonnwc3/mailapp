import {Schema, arrayOf} from 'normalizr';

export const contact       = new Schema('contacts', {idAttribute: '_id'});
export const address       = new Schema('addresses', {idAttribute: '_id'});
export const communication = new Schema('communications', {idAttribute: '_id'});

contact.define({
  addresses     : arrayOf(address),
  communications: arrayOf(communication)
});