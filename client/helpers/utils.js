import {OrderedMap} from 'immutable';

export const objectId = (
  m = Math,
  d = Date,
  h = 16,
  s = s => m.floor(s).toString(h)
) => s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));

export function arrayToMap(array, Model) {

  return array
    .map(item => clearItem(item))
    .reduce((acc, item) => {
      return acc.set(item._id, new Model(item));
    }, new OrderedMap({}));

}

export function objToMap(obj = {}, Model) {
  return Object.keys(obj)
    .reduce((acc, key) => {
      return acc.set(obj[key]._id, new Model(clearItem(obj[key])));
    }, new OrderedMap());
}

export function clearItem(item) {
  Object.keys(item).forEach(key => {
    if (item[key] === null || item[key] === undefined) {
      item[key] = '';
    }
  });
  return item;
}

export function clearId(item) {
  delete item._id;
  return item;
}

export function hasClass(el, cls) {
  return el.className &&
    new RegExp('(\\s|^)' + cls + '(\\s|$)').test(el.className);
}

export function required(val) {
  if (val === null || val === undefined || val === '') {
    return 'Value is required';
  }
  return null;
}
