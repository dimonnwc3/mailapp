export function checkAuth(store) {
  return (nextState, replace) => {
    console.log('checkAuth');
    const isLogged = store.getState().auth.isLogged;
    if (!isLogged) replace(`/auth?redirectTo=${nextState.location.pathname}`);
  };
}

export function checkNoAuth(store) {
  return (nextState, replace) => {
    console.log('checkNoAuth');
    const isLogged = store.getState().auth.isLogged;
    if (isLogged) replace(nextState.location.query || '/');
  };
}
