import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const Contact = (props) => {

  const {contact: c} = props;
  const url = `/contacts/edit/${c._id}`;
  const content = `${c.firstName} ${c.lastName}`;

  return <Link to={url}>{content || c._id}</Link>;

};

Contact.propTypes = {
  contact: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    firstName: PropTypes.string,
    lastName: PropTypes.string
  })
};

export default Contact;