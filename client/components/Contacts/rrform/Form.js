import React, {Component} from 'react';
import {Well, Form, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router';
// import {deleteContact} from '../../../AC/contacts';
import FieldGroup from '../../Common/FieldGroupReact';

import {LocalForm, Control, actions} from 'react-redux-form';

class CForm extends Component {
  static propTypes = {};

  componentWillReceiveProps(nextProps) {
    if (nextProps.contact._id !== this.props.contact._id) {
      this.formDispatch(actions.load('local', nextProps.contact));
    }
  }

  state = {
    form: null
  }

  handleChange = (values) => {
    console.log('handleChange', values);
  }
  handleUpdate = (form) => {
    this.setState({form});
    console.log('handleUpdate', form);
  }
  handleSubmit = (values) => {
    console.log('handleSubmit', values);
  }

  render() {

    const {contact} = this.props;
    const {form} = this.state;

    const legend =
      <legend>
        Edit contact: {`${contact.firstName} ${contact.lastName}`}
      </legend>;

    const avatar =
      <a className='thumbnail' href={contact.avatarUrl}>
        <img src={contact.avatarUrl} className='img-responsive'/>
      </a>;

    const firstName =
      <Control
        model='.firstName'
        component={FieldGroup}
        validators={{
          required: (val) => val && val.length
        }}
        mapProps={{
          name: (props) => props.model,
          onBlur: (props) => props.onBlur,
          onChange: (props) => props.onChange,
          onFocus: (props) => props.onFocus,
          onKeyPress: (props) => props.onKeyPress,
          value: (props) => props.modelValue,
          meta: (props) => props.fieldValue
        }}
        controlProps={{
          form,
          placeholder: 'Enter text',
          label: 'First name',
          id: 'firstName'
        }}
      />;

    return (
      <div>

        <Form
          componentClass={LocalForm}
          initialState={contact}
          getDispatch={(dispatch) => this.formDispatch = dispatch}
          onUpdate={this.handleUpdate}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
        >

          <Row>

            <Col sm={4}>

              <Well>
                <fieldset>
                  {legend}

                  <Row >
                    <Col md={5}>{avatar}</Col>
                    <Col md={7}></Col>
                  </Row>

                  {firstName}

                </fieldset>
              </Well>

              <Well>
                <fieldset>
                  <div className='form-group'>
                    <div className='pull-right'>
                      <Control.button
                        model='user'
                        className='btn btn-primary'
                        disabled={() => {
                          return form &&
                            (!form.$form.valid || form.$form.pristine);
                        }}
                      >
                        Save
                      </Control.button>
                      <Link className='btn btn-link'
                            to='/contacts'
                      >
                        Cancel
                      </Link>
                      {contact._id &&
                      <button type='button'
                              className='btn btn-link'
                              onClick={this.handleDelete}>
                        Remove
                      </button>}
                    </div>
                  </div>
                </fieldset>
              </Well>

            </Col>
            <Col sm={4}>

            </Col>
            <Col sm={4}>

            </Col>

          </Row>


        </Form>

      </div>
    );
  }
}

export default CForm;
