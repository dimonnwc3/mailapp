import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {createContact} from '../../AC/contacts';
import ContactForm from './Form/Form';
import {Row, Col} from 'react-bootstrap';

class Add extends React.Component {
  static propTypes = {
    creating: PropTypes.bool.isRequired,
    createContact: PropTypes.func.isRequired
  };

  componentWillReceiveProps(nextProps) {
    const {creating} = nextProps;
    const {router}   = this.context;
    if (!creating && this.props.creating) return router.push('/contacts');
  }

  static contextTypes = {
    router: PropTypes.object
  }

  render() {
    const {creating} = this.props;

    return (
      <Row className='selected-contact'>
        <Col sm={12}>
          <ContactForm disabled={creating}
                       onSubmit={this.handleSubmit}
          />
        </Col>
      </Row>
    );
  }

  handleSubmit = (data) => this.props.createContact(data)

}

export default connect(({contacts}) => {
  return {
    creating: contacts.creating
  };
}, {createContact})(Add);
