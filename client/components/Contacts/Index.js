import React from 'react';

const Index = () => {
  return (
    <div className='row select-contact-text'>
      <div className='col-xs-offset-3 col-xs-9'>
        <h1 className='text-muted'>Select a contact</h1>
      </div>
    </div>
  );
};

export default Index;
