import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {getContacts} from '../../AC/contacts';
import Loader from '../Common/Loader';

import {values} from 'lodash';

import Contact from './Single';

class ContactList extends React.Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    loaded: PropTypes.bool.isRequired,
    contacts: PropTypes.array.isRequired,
    getContacts: PropTypes.func.isRequired,
    searchTerm: PropTypes.string.isRequired
  };

  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount() {
    const {getContacts}     = this.props;
    const {loading, loaded} = this.props;
    if (!loading && !loaded) getContacts();
  }

  render() {

    const {isActive}                      = this.context.router;
    const {contacts, loading, searchTerm} = this.props;

    if (loading) return <Loader/>;

    const contactsBody = contacts.map(c =>
      <li key={c._id}
          className={isActive(`/contacts/edit/${c._id}`) ? 'active' : ''}>
        <Contact contact={c}/>
      </li>
    );

    if (!contactsBody.length) return (
      <div>
        No matches for «{searchTerm}»
      </div>
    );

    return (
      <ul className='nav nav-sidebar'>
        {contactsBody}
      </ul>
    );
  }

}

export default connect((state) => {
  const filter = state.contacts.filter.toLowerCase();

  const contacts = values(state.contacts.entities)
    .filter(c => c.firstName && c.lastName)
    .filter(
      c => startsWith(c.firstName, filter) || startsWith(c.lastName, filter)
    )
    .sort((a, b) => b.lastName.toLowerCase() - a.lastName.toLowerCase());

  return {
    loading: state.contacts.loading,
    loaded: state.contacts.loaded,
    contacts,
    searchTerm: state.contacts.filter
  };
}, {getContacts}, null, {pure: false})(ContactList);

function startsWith(str, filter) {
  return str.toLowerCase().startsWith(filter);
}