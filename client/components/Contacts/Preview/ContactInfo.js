import React from 'react';
import ContactInfoItem from './ContactInfoItem';

export default function ContactInfo(props) {
    const { infoItems } = props;
    const infoItemsList = infoItems.map(item =>
      <li key={item._id} className="list-group-item">
        <ContactInfoItem info={item} />
      </li>);

    return (
      <ul className="list-group contact-preview-listgroup">
        {infoItemsList}
      </ul>
    );
}
