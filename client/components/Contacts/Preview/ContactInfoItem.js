import React from 'react';

function ContactInfoItem(props) {
  const { info: { comment, content, type } } = props;
  return (
    <div>
      <p>{type} {content}</p>
      <i>{comment}</i>
    </div>
  );
}
export default ContactInfoItem;
