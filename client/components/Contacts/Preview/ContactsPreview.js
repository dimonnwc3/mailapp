import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getContact} from '../../../AC/contacts';
import Loader from '../../Common/Loader';
import ContactInfo from './ContactInfo';
import AddressInfo from './AddressInfo';
import {Link} from 'react-router';
import './styles.css';

class ContactsPreview extends Component {

  static propTypes = {
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    contact: PropTypes.object,
    getContact: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired
  };

  componentWillReceiveProps(nextProps) {
    const {params, getContact, loading, loaded} = nextProps;
    if (loading || loaded || !this.props.loaded) return;
    getContact(params.id);
  }

  componentDidMount() {
    const {params, getContact, loading, loaded} = this.props;
    if (loaded || loading) return;
    getContact(params.id);
  }

  render() {
    const {loaded, loading, contact} = this.props;
    if (!loaded || loading) return <Loader />;

    const {
      avatarUrl,
      firstName,
      lastName,
      dob,
      communications,
      addresses,
      _id,
      comment
    } = contact;

    const contactsPresent = communications.length ?
      <ContactInfo infoItems={communications}/> : <p>Nothing here yet</p>;
    const addressesPresent = addresses.length ?
      <AddressInfo infoItems={addresses}/> : <p>Nothing here yet</p>;

    return (
      <div className="row selected-contact">
        <div className="col-xs-12 col-sm-12 col-md-9">
          <div className="row">
            <div className="col-xs-4  contact-preview-profile">
              <div className="well">
                <img src={avatarUrl} alt="avatar"/>
                <h3>{firstName} {lastName}</h3>
                <p>{dob}</p>
                <button type="button" className="btn btn-primary">
                  <Link to={`/contacts/edit/${_id}`}>Edit contact</Link>
                </button>
              </div>
              <div className="well">
                <h4>Note on {firstName}</h4>
                <p className="contact-preview-note">{comment}</p>
              </div>
            </div>
            <div className="col-xs-8">
              <div className="well">
                <h3 className="contact-preview-header">Contact Info</h3>
                <div>
                  {contactsPresent}
                </div>
                <h3 className="contact-preview-header">Addresses</h3>
                <div>
                  {addressesPresent}
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default connect(({contacts, communications, addresses}, props) => {
  const contact = contacts.entities[props.params.id] || {};

  if (contact && Object.keys(contact).length) {
    contact.communications = contact.communications
      .map(id => communications.entities[id]);

    contact.addresses = contact.addresses
      .map(id => addresses.entities[id]);
  }

  return {
    contact: contact,
    loading: contact.loading,
    loaded: contact.loaded
  };
}, {getContact})(ContactsPreview);
