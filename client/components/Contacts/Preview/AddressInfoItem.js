import React from 'react';

function AddressInfoItem(props) {
  const { info: { city, comment, country, houseNo,
  street, type, zip } } = props;
  return (
    <div>
      <h4>{type}</h4>
      <p>{houseNo} {street}</p>
      <p>{city} {country}</p>
      <p>{zip}</p>
      <i>{comment}</i>
    </div>
  );
}
export default AddressInfoItem;
