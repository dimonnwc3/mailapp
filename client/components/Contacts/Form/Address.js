import React, {PropTypes} from 'react';
import {Field, FormSection} from 'redux-form';
import FieldGroup from '../../Common/FieldGroup';
import {Well, Row, Col, Button} from 'react-bootstrap';
import {required} from '../../../helpers/utils';

const Address = (props) => {

  const {address, onRemove} = props;

  const legend = <legend className='col-xs-4'>Address</legend>;

  const type =
    <Col xs={8}>
      <Field
        id={'type-' + address._id}
        name='type'
        type='text'
        label='Type'
        placeholder='Enter text'
        componentClass='select'
        component={FieldGroup}
        validate={required}
      >
        <option value='Postal'>Postal</option>
        <option value='Home'>Home</option>
        <option value='Office'>Office</option>
      </Field>
    </Col>;

  const zip =
    <Field
      name='zip'
      id={'zip-' + address._id}
      type='text'
      label='Zip'
      placeholder='Enter text'
      component={FieldGroup}
      validate={required}
    />;

  const city =
    <Field
      id={'city-' + address._id}
      name='city'
      type='text'
      label='City'
      placeholder='Enter text'
      component={FieldGroup}
      validate={required}
    />;

  const street =
    <Field
      id={'street-' + address._id}
      name='street'
      type='text'
      label='Street'
      placeholder='Enter text'
      component={FieldGroup}
      validate={required}
    />;

  const houseNo =
    <Field
      id={'houseNo-' + address._id}
      name='houseNo'
      type='text'
      label='HouseNo'
      placeholder='Enter text'
      component={FieldGroup}
      validate={required}
    />;

  return (
    <Well className='pos-r'>
      <fieldset>
        <Row>
          {legend}
          {type}
        </Row>

        <Row>
          <Col xs={6}>{zip}</Col>
          <Col xs={6}>{city}</Col>
        </Row>

        <Row >
          <Col xs={7}>{street}</Col>
          <Col xs={5}>{houseNo}</Col>
        </Row>

      </fieldset>
      <Button type='button'
              className='close btn-rm'
              onClick={onRemove}>
        &times;
      </Button>
    </Well>
  );
};

Address.propTypes = {
  address: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired
};

const Addresses = (props) => {

  const {fields} = props;

  const body = fields.map((address, idx) => {
    return (
      <FormSection key={idx} name={address}>
        <Address address={fields.get(idx)}
                 onRemove={() => fields.remove(idx)}
        />
      </FormSection>
    );
  });

  return (
    <div>
      <Row>
        <Col xs={12}>
            <span className='pull-right'>
              <i className='fa fa-plus btn-add'
                 onClick={() => fields.push({type: 'Postal'})}
              />
            </span>
        </Col>
      </Row>
      {body}
    </div>
  );

};

Addresses.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired
};

export {Address, Addresses};
