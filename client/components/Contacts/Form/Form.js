import React, {Component, PropTypes} from 'react';
import {reduxForm, Field, FieldArray, getFormValues} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {Addresses} from './Address';
import {Communications} from './Communication';
import {deleteContact} from '../../../AC/contacts';
import FieldGroup from '../../Common/FieldGroup';
import {required} from '../../../helpers/utils';
import {Well, Form, Row, Col} from 'react-bootstrap';

class ContactForm extends Component {
  static propTypes = {
    contact: PropTypes.object.isRequired,
    originalContact: PropTypes.object.isRequired,
    deleteContact: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    updating: PropTypes.bool
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.updating && this.props.updating) {
      nextProps.initialize(nextProps.contact);
    }
    if (nextProps.originalContact._id !== this.props.originalContact._id) {
      nextProps.initialize(nextProps.originalContact);
    }
  }

  render() {

    const {handleSubmit, contact, disabled} = this.props;
    const {pristine, invalid} = this.props;

    const legend =
      <legend>
        Edit contact: {`${contact.firstName} ${contact.lastName}`}
      </legend>;

    const avatar =
      <a className='thumbnail' href={contact.avatarUrl}>
        <img src={contact.avatarUrl} className='img-responsive'/>
      </a>;

    const avatarUrl =
      <Field
        id='avatarUrl'
        name='avatarUrl'
        type='text'
        label='Avatar'
        placeholder='Enter text'
        component={FieldGroup}
        validate={required}
      />;

    const firstName =
      <Field
        name='firstName'
        id='firstName'
        type='text'
        label='First name'
        placeholder='Enter text'
        component={FieldGroup}
        validate={required}
      />;

    const lastName =
      <Field
        name='lastName'
        id='lastName'
        type='text'
        label='Last name'
        placeholder='Enter text'
        component={FieldGroup}
        validate={required}
      />;

    const dob =
      <Field
        leftAddon={<i className='fa fa-birthday-cake'/>}
        name='dob'
        id='dob'
        type='text'
        label='Dob'
        placeholder='Enter text'
        component={FieldGroup}
        validate={required}
      />;

    const comment =
      <Field
        label='Comment'
        rows='3'
        name='comment'
        id='comment'
        componentClass='textarea'
        component={FieldGroup}
      />;

    return (
      <Form name='contactForm' onSubmit={handleSubmit}>
        <Row>

          <Col sm={4}>

            <Well>
              <fieldset>
                {legend}

                <Row >
                  <Col md={5}>{avatar}</Col>
                  <Col md={7}>{avatarUrl}</Col>
                </Row>

                {firstName}
                {lastName}
                {dob}
                {comment}

              </fieldset>
            </Well>

            <Well>
              <fieldset>
                <div className='form-group'>
                  <div className='pull-right'>
                    <button type='submit'
                            className='btn btn-primary'
                            disabled={disabled || pristine || invalid}>
                      {disabled ? 'Saving...' : 'Save'}
                    </button>
                    <Link className='btn btn-link'
                          to='/contacts'
                          disabled={disabled}>
                      Cancel
                    </Link>
                    {contact._id &&
                    <button type='button'
                            className='btn btn-link'
                            disabled={disabled}
                            onClick={this.handleDelete}>
                      Remove
                    </button>}
                  </div>
                </div>
              </fieldset>
            </Well>

          </Col>
          <Col sm={4}>
            <FieldArray name='addresses' component={Addresses}/>
          </Col>
          <Col sm={4}>
            <FieldArray name='communications' component={Communications}/>
          </Col>

        </Row>
      </Form>
    );
  }

  handleDelete = () => this.props.deleteContact(this.props.contact._id);

}

export default connect((state, props) => {
  return {
    contact: getFormValues('contact')(state) || props.contact || {},
    originalContact: props.contact || {}
  };
}, {deleteContact})(reduxForm({form: 'contact'})(ContactForm));

