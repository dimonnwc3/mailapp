import React, {PropTypes} from 'react';
import {Field, FormSection} from 'redux-form';
import FieldGroup from '../../Common/FieldGroup';
import {Well, Row, Col, Button} from 'react-bootstrap';
import {required} from '../../../helpers/utils';

const Communication = (props) => {

  const {communication, onRemove} = props;

  const legend = <legend className='col-xs-6'>Communication</legend>;

  const type =
    <Col xs={6}>
      <Field
        id={'type-' + communication._id}
        name='type'
        type='text'
        label='Type'
        placeholder='Enter text'
        componentClass='select'
        component={FieldGroup}
        validate={required}
      >
        <option value='Email'>Email</option>
        <option value='Phone'>Phone</option>
        <option value='Web-Site'>Web-Site</option>
      </Field>
    </Col>;

  const content =
    <Field
      id={'content-' + communication._id}
      name='content'
      type='text'
      label={communication.type}
      placeholder='Enter text'
      component={FieldGroup}
      validate={required}
    />;

  const comment =
    <Field
      label='Comment'
      rows='3'
      name='comment'
      id={'comment-' + communication._id}
      componentClass='textarea'
      component={FieldGroup}
    />;

  return (
    <Well className='pos-r'>
      <fieldset>
        <Row>
          {legend}
          {type}
        </Row>

        {content}
        {comment}

      </fieldset>
      <Button type='button'
              className='close btn-rm'
              onClick={onRemove}>
        &times;
      </Button>
    </Well>
  );
};

Communication.propTypes = {
  communication: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired
};

const Communications = (props) => {

  const {fields} = props;

  const body = fields.map((communication, idx) => {
    return (
      <FormSection key={idx} name={communication}>
        <Communication communication={fields.get(idx)}
                       onRemove={() => fields.remove(idx)}
        />
      </FormSection>
    );
  });

  return (
    <div>
      <Row>
        <Col xs={12}>
            <span className='pull-right'>
              <i className='fa fa-plus btn-add'
                 onClick={() => fields.push({type: 'Phone'})}
              />
            </span>
        </Col>
      </Row>
      {body}
    </div>
  );

};

Communications.propTypes = {
  fields: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired
};

export {Communication, Communications};
