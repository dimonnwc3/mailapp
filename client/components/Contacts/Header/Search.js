import React, {Component} from 'react';
import {connect} from 'react-redux';
import {filterContacts} from '../../../AC/contacts';
import {hasClass} from '../../../helpers/utils';

class Search extends Component {

  onInputChange = (e) => {
    const { filterContacts } = this.props;
    hasClass(e.target, 'close-btn') ?
      filterContacts('') : filterContacts(e.target.value);
  }

  render() {
    return (
      <div className='input-group'>
        <span className="input-group-addon">
          Search
        </span>
        <input type='text'
               className='form-control'
               onChange={this.onInputChange}
               value={this.props.term}
               autoFocus/>
        <span
          className="input-group-addon glyphicon glyphicon-remove close-btn"
          onClick={this.onInputChange}
          style={{top: '0px', background: 'white',
            color: 'gray', cursor: 'pointer'}} />
      </div>
    );
  }
}

export default connect(({contacts}) => ({
  term: contacts.filter
}), {filterContacts})(Search);
