import React from 'react';
import {Link} from 'react-router';
import Search from './Search';

const Header = () => {
  return (
    <div className='row'>
      <div className='col-xs-4'>
        <Link className='btn btn-primary' to='/contacts/create'>
          Add new contact
        </Link>
      </div>
      <div className='col-xs-8'>
        <Search/>
      </div>
    </div>
  );
};

export default Header;