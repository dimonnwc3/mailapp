import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const ContactButtons = (props) => {
  const {updating, deleting, creating, isNew, onDelete} = props;

  const submit =
          <button type='submit'
                  className='btn btn-primary'
                  disabled={updating || creating}>
            Save
          </button>;

  const remove = isNew ?
    <button className='btn btn-link'
            onClick={onDelete}
            disabled={deleting}>
      Remove
    </button> : null;

  return (
    <div className='well'>
      <fieldset>
        <div className='form-group'>
          <div className='pull-right'>
            {submit}
            <Link className='btn btn-link' to='/contacts'>Cancel</Link>
            {remove}
          </div>
        </div>
      </fieldset>
    </div>
  );
};

ContactButtons.propTypes = {
  updating: PropTypes.bool,
  deleting: PropTypes.bool,
  creating: PropTypes.bool,
  onDelete: PropTypes.func.isRequired
};

export default ContactButtons;
