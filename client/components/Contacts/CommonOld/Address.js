import React, {PropTypes} from 'react';
import BtnRemove from '../../Common/BtnRemove/Index';

class ContactAddress extends React.Component {
  static propTypes = {
    address: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    onRemove: PropTypes.func
  };

  render() {
    const {address, handleChange, onRemove} = this.props;

    const legend = <legend className='col-xs-4'>Address</legend>;

    const type =
            <div className='form-group col-xs-8'>
              <label htmlFor={'type-' + address._id}
                     className='control-label'>Type</label>
              <select className='form-control'
                      id={'type-' + address._id}
                      name='type'
                      value={address.type || ''}
                      onChange={handleChange}>
                <option value='Postal'>Postal</option>
                <option value='Home'>Home</option>
                <option value='Office'>Office</option>
              </select>
            </div>;

    const zip =
            <div className='form-group'>
              <label htmlFor={'zip-' + address._id}
                     className='control-label'>Zip</label>
              <input type='text'
                     className='form-control'
                     id={'zip-' + address._id}
                     name='zip'
                     value={address.zip || ''}
                     onChange={handleChange}/>
            </div>;

    const city =
            <div className='form-group'>
              <label htmlFor={'city-' + address._id}
                     className='control-label'>City</label>
              <input type='text'
                     className='form-control'
                     id={'city-' + address._id}
                     name='city'
                     value={address.city || ''}
                     onChange={handleChange}/>
            </div>;

    const street =
            <div className='form-group'>
              <label htmlFor={'street-' + address._id}
                     className='control-label'>Street</label>
              <input type='text'
                     className='form-control'
                     id={'street-' + address._id}
                     name='street'
                     value={address.street || ''}
                     onChange={handleChange}/>
            </div>;

    const houseNo =
            <div className='form-group'>
              <label htmlFor={'houseNo-' + address._id}
                     className='control-label'>House No</label>
              <input type='text'
                     className='form-control'
                     id={'houseNo-' + address._id}
                     name='houseNo'
                     value={address.houseNo || ''}
                     onChange={handleChange}/>
            </div>;

    return (
      <div className="well pos-r">
        <fieldset>
          <div className="row">
            {legend}
            {type}
          </div>

          <div className="row">
            <div className="col-xs-6">{zip}</div>
            <div className="col-xs-6">{city}</div>
          </div>

          <div className="row">
            <div className="col-xs-7">{street}</div>
            <div className="col-xs-5">{houseNo}</div>
          </div>

        </fieldset>
        <BtnRemove remove={onRemove}/>
      </div>
    );
  }

}

export default ContactAddress;
