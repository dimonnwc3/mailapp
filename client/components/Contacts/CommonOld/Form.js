import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Communication, Address} from '../../../records';
import {objectId} from '../../../helpers/utils';

import ContactInfo from './Info';
import ContactAddress from './Address';
import ContactCommunication from './Communication';
import ContactButtons from './Buttons';
import BtnAdd from '../../Common/BtnAdd/BtnAdd';

import {
  updateContact,
  deleteContact,
  createContact
} from '../../../AC/contacts';

const propsMap = {
  addresses    : Address,
  communications: Communication
};

class ContactForm extends React.Component {
  static propTypes = {
    contact      : PropTypes.object.isRequired,
    updating     : PropTypes.bool,
    deleting     : PropTypes.bool,
    creating     : PropTypes.bool,
    updateContact: PropTypes.func.isRequired,
    createContact: PropTypes.func.isRequired,
    deleteContact: PropTypes.func.isRequired
  };

  state = {
    contact: this.props.contact
  }

  componentWillReceiveProps(props) {
    this.setState({contact: props.contact});
  }

  render() {
    const {contact}                      = this.state;
    const {updating, deleting, creating} = this.props;

    const addresses = contact.addresses.map(a =>
      <div key={a._id}>
        <ContactAddress
          handleChange={this.handleArrayChange('addresses', a._id)}
          address={a}
          onRemove={this.removeArrayItem('addresses', a._id)}/>
      </div>
    );

    const communications = contact.communications.map(c =>
      <div key={c._id}>
        <ContactCommunication
          handleChange={this.handleArrayChange('communications', c._id)}
          communication={c}
          onRemove={this.removeArrayItem('communications', c._id)}/>
      </div>
    );

    return (
      <form name='contactForm' onSubmit={this.handleSubmit}>
        <div className="row">

          <div className="col-sm-4">
            <ContactInfo contact={contact}
                         handleChange={this.handleFieldChange}/>
            <ContactButtons updating={updating}
                            deleting={deleting}
                            creating={creating}
                            isNew={Boolean(contact._id)}
                            onDelete={this.handleDelete}/>
          </div>
          <div className="col-sm-4">
            {this.getWrappedBtn('addresses')}
            {addresses}
          </div>
          <div className="col-sm-4">
            {this.getWrappedBtn('communications')}
            {communications}
          </div>

        </div>
      </form>
    );
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const {contact, contact: {_id, isNew}} = this.state;

    if (isNew) {
      this.props.createContact(contact);
    } else {
      this.props.updateContact(_id, contact);
    }
  }

  handleDelete = (e) => {
    e.preventDefault();
    const {_id} = this.state.contact;
    this.props.deleteContact(_id);
  }

  handleFieldChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      contact: this.state.contact.set(name, value)
    });
  }

  handleArrayChange(prop, id) {
    return (e) => {
      const {name, value} = e.target;
      const {contact}     = this.state;
      const idx = contact[prop].findIndex(a => a._id === id);

      this.setState({
        contact: contact.setIn([prop, idx, name], value)
      });

    };
  }

  removeArrayItem = (prop, id) => {
    return (e) => {
      e.preventDefault();
      const {contact} = this.state;

      this.setState({
        contact: contact.update(prop, p => p.filter(a => a._id !== id))
      });

    };
  }

  addArrayItem = (prop) => {
    return (e) => {
      e.preventDefault();
      const {contact} = this.state;

      this.setState({
        contact: contact.update(prop, p => p.push(new propsMap[prop]({
          _id: objectId()
        })))
      });
    };
  }

  getWrappedBtn(forProp) {
    return (
      <div className="row">
        <div className="col-xs-12">
        <span className='pull-right'>
          <BtnAdd add={this.addArrayItem(forProp)}/>
        </span>
        </div>
      </div>
    );
  }

}

export default connect(null, {
  updateContact,
  deleteContact,
  createContact
})(ContactForm);
