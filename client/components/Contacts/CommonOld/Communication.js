import React, {PropTypes} from 'react';
import BtnRemove from '../../Common/BtnRemove/Index';

class ContactCommunication extends React.Component {
  static propTypes = {
    communication: PropTypes.object.isRequired,
    handleChange : PropTypes.func.isRequired,
    onRemove: PropTypes.func
  };

  render() {
    const {communication, handleChange, onRemove} = this.props;

    const legend = <legend className='col-xs-6'>Communication</legend>;

    const type =
            <div className='form-group col-xs-6'>
              <label htmlFor={'type-' + communication._id}
                     className='control-label'>Type</label>
              <select className='form-control'
                      id={'type-' + communication._id}
                      name='type'
                      value={communication.type || ''}
                      onChange={handleChange}>
                <option value='Email'>Email</option>
                <option value='Phone'>Phone</option>
                <option value='Web-Site'>Web-Site</option>
              </select>
            </div>;

    const content =
            <div className='form-group'>
              <label htmlFor={'content-' + communication._id}
                     className='control-label'>{communication.type}</label>
              <input type='text'
                     className='form-control'
                     id={'content-' + communication._id}
                     name='content'
                     value={communication.content || ''}
                     onChange={handleChange}/>
            </div>;

    const comment =
            <div className='form-group'>
              <label htmlFor={'comment-' + communication._id}
                     className='control-label'>Comment</label>
              <textarea className='form-control'
                        name='comment'
                        rows='3'
                        id={'comment-' + communication._id}
                        value={communication.comment || ''}
                        onChange={handleChange}/>
            </div>;

    return (
      <div className="well pos-r">
        <fieldset>
          <div className="row">
            {legend}
            {type}
          </div>

          {content}
          {comment}

        </fieldset>
        <BtnRemove remove={onRemove}/>
      </div>
    );
  }

}

export default ContactCommunication;
