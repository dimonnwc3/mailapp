import React, {PropTypes} from 'react';

class ContactInfo extends React.Component {
  static propTypes = {
    contact     : PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired
  };

  render() {

    const {contact, handleChange} = this.props;

    const legend =
            <legend>
              Edit contact: {`${contact.firstName} ${contact.lastName}`}
            </legend>;

    const avatar =
            <a className='thumbnail' href={contact.avatarUrl}>
              <img src={contact.avatarUrl} className='img-responsive'/>
            </a>;

    const avatarUrl =
            <div className='form-group'>
              <label htmlFor='avatarUrl'
                     className='control-label'>Avatar url</label>
              <div className='input-group'>
                        <span className='input-group-addon'><i
                          className='fa fa-link'/>
                        </span>
                <input type='text' className='form-control'
                       id='avatarUrl'
                       onChange={handleChange}
                       name='avatarUrl' value={contact.avatarUrl}/>
              </div>
            </div>;

    const firstName =
            <div className='form-group'>
              <label htmlFor='firstName' className='control-label'>First
                                                                   name</label>
              <input type='text' className='form-control' id='firstName'
                     onChange={handleChange}
                     name='firstName' required value={contact.firstName}/>
            </div>;

    const lastName =
            <div className='form-group'>
              <label htmlFor='lastName' className='control-label'>Last
                                                                  name</label>
              <input type='text' className='form-control' id='lastName'
                     onChange={handleChange}
                     name='lastName' required value={contact.lastName}/>
            </div>;

    const dob =
            <div className='form-group'>
              <label htmlFor='dob'
                     className='control-label'>Dob</label>
              <div className='input-group'>
                        <span className='input-group-addon'><i
                          className='fa fa-birthday-cake'/>
                        </span>
                <input type='text' className='form-control' id='dob'
                       onChange={handleChange}
                       name='dob' value={contact.dob}/>
              </div>
            </div>;

    const comment =
            <div className='form-group'>
              <label htmlFor='comment'
                     className='control-label'>Comment</label>
              <textarea className='form-control' name='comment'
                        rows='3'
                        id='comment'
                        value={contact.comment}
                        onChange={handleChange}/>
            </div>;

    return (
      <div className="well">
        <fieldset>
          {legend}

          <div className="row">
            <div className="col-md-5">{avatar}</div>
            <div className="col-md-7">{avatarUrl}</div>
          </div>

          {firstName}
          {lastName}
          {dob}
          {comment}

        </fieldset>
      </div>
    );

  }

}

export default ContactInfo;
