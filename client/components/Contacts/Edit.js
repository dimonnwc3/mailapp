import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {getContact, updateContact} from '../../AC/contacts';
import Loader from '../Common/Loader';
import ContactForm from './Form/Form';
// import ContactForm from './rrform/Form';
import {Row, Col} from 'react-bootstrap';

class Edit extends React.Component {
  static propTypes = {
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    deleting: PropTypes.bool,
    updating: PropTypes.bool,
    contact: PropTypes.object,
    getContact: PropTypes.func.isRequired,
    updateContact: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired
  };

  componentWillReceiveProps(nextProps) {
    const {params, getContact, loading, loaded, deleting} = nextProps;
    const {router} = this.context;
    if (!deleting && this.props.deleting) return router.push('/contacts');
    if (loading || loaded || !this.props.loaded) return;
    getContact(params.id);
  }

  componentDidMount() {
    const {params, getContact, loading, loaded} = this.props;
    if (loaded || loading) return;
    getContact(params.id);
  }

  static contextTypes = {
    router: PropTypes.object
  }

  render() {

    const {contact, loading, loaded, deleting, updating} = this.props;

    if (loading || !loaded) return <Loader/>;

    return (
      <Row className='selected-contact'>
        <Col sm={123}>
          <ContactForm contact={contact}
                       initialValues={contact}
                       disabled={updating || deleting}
                       updating={updating}
                       onSubmit={this.handleSubmit}
          />
        </Col>
      </Row>
    );
  }

  handleSubmit = (data) => this.props.updateContact(data._id, data)

}

export default connect(({contacts, communications, addresses}, props) => {

  const loading = contacts.entities[props.params.id] &&
    contacts.entities[props.params.id].loading;

  const loaded = contacts.entities[props.params.id] &&
    contacts.entities[props.params.id].loaded;

  const contact = loaded && !loading ?
    {...contacts.entities[props.params.id]} : null;

  if (contact) {
    contact.communications = contact.communications
      .map(id => communications.entities[id]);

    contact.addresses = contact.addresses
      .map(id => addresses.entities[id]);
  }

  return {
    contact,
    loading,
    loaded,
    deleting: contacts.entities[props.params.id] &&
    contacts.entities[props.params.id].deleting || false,
    updating: contacts.entities[props.params.id] &&
    contacts.entities[props.params.id].updating || false
  };
}, {getContact, updateContact})(Edit);
