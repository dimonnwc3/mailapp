import React from 'react';

const Loader = () => {
  return (
    <div className="loader-wrapper">
      <div className='loader'>
        <i className='fa fa-5x fa-spinner fa-spin'/>
      </div>
    </div>
  );
};

export default Loader;
