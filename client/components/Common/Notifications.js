import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NotificationStack} from 'react-notification';
import {deleteNotification} from '../../AC/notifications';

class Notifications extends Component {

  onDismiss = (notification) => {
    this.props.deleteNotification(notification);
  }

  render() {
    const {notifications} = this.props;
    if (!notifications.length) return null;

    return (
      <NotificationStack
        notifications={notifications}
        onDismiss={this.onDismiss}
      />
    );
  }
}

export default connect(({notifications}) => ({
  notifications: notifications.entities
}), {deleteNotification})(Notifications);
