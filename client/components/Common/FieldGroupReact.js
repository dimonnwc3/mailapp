import React from 'react';
import cn from 'classnames';
import {
  HelpBlock,
  FormGroup,
  ControlLabel,
  FormControl,
  InputGroup
} from 'react-bootstrap';
import {Errors} from 'react-redux-form';

function FieldGroup(props) {
  const {
    disabled, name, onBlur, onChange, onFocus, onKeyPress, value,
    leftAddon, rightAddon, id, label, placeholder, meta,
    ...anotherProps
  } = props;

  const {valid, touched, pristine} = meta;

  const validationState = cn({
    'error': (!pristine && touched && !valid),
    'success': (!pristine && touched && valid)
  });

  const control = leftAddon || rightAddon ?
    <InputGroup>
      {leftAddon && <InputGroup.Addon>{leftAddon}</InputGroup.Addon>}
      <FormControl
        disabled={disabled}
        name={name}
        onBlur={onBlur}
        onChange={onChange}
        onFocus={onFocus}
        value={value}
        placeholder={placeholder}
        onKeyPress={onKeyPress}
        {...anotherProps}
      />
      {rightAddon && <InputGroup.Addon>{rightAddon}</InputGroup.Addon>}
    </InputGroup> :
    <FormControl
      disabled={disabled}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      onFocus={onFocus}
      value={value}
      placeholder={placeholder}
      onKeyPress={onKeyPress}
      {...anotherProps}
    />;

  return (
    <FormGroup validationState={validationState || null} controlId={id}>

      <ControlLabel>{label}</ControlLabel>

      {control}

      <Errors
        model={name}
        show='touched'
        component={(props) => <HelpBlock>{props.children}</HelpBlock>}
        messages={{
          required: 'Required',
        }}
      />

    </FormGroup>
  );
}

export default FieldGroup;
