import React from 'react';
import cn from 'classnames';
import {
  HelpBlock,
  FormGroup,
  ControlLabel,
  FormControl,
  InputGroup
} from 'react-bootstrap';

function FieldGroup({id, label, input, meta, leftAddon, rightAddon, ...props}) {
  const {error, invalid, valid, touched, dirty} = meta;

  const validationState = cn({
    'error': (dirty && touched && invalid),
    'success': (dirty && touched && valid)
  });

  const control = leftAddon || rightAddon ?
    <InputGroup>
      {leftAddon && <InputGroup.Addon>{leftAddon}</InputGroup.Addon>}
      <FormControl {...input} {...props}/>
      {rightAddon && <InputGroup.Addon>{rightAddon}</InputGroup.Addon>}
    </InputGroup> :
    <FormControl {...input} {...props}/>;

  return (
    <FormGroup validationState={validationState || null} controlId={id}>

      <ControlLabel>{label}</ControlLabel>

      {control}

      {(dirty && touched) && invalid && error && <HelpBlock>{error}</HelpBlock>}

    </FormGroup>
  );
}

export default FieldGroup;
