import React from 'react';
import routes from '../../routes';
import {Router, browserHistory} from 'react-router';
import {bindActionCreators} from 'redux';

import {Provider} from 'react-redux';
import createStore from '../../store';

const initialState = window.REDUX_INITIAL_STATE || {};

const store = createStore(initialState);
window.store = store;

import * as auth from '../../AC/auth';
import * as contacts from '../../AC/contacts';
import * as folders from '../../AC/folders';
import * as mail from '../../AC/mail';
import * as notifications from '../../AC/notifications';
import * as user from '../../AC/user';

window.actions = {
  auth: bindActionCreators(auth, store.dispatch),
  contacts: bindActionCreators(contacts, store.dispatch),
  folders: bindActionCreators(folders, store.dispatch),
  mail: bindActionCreators(mail, store.dispatch),
  notifications: bindActionCreators(notifications, store.dispatch),
  user: bindActionCreators(user, store.dispatch),
};

const App = () => {
  return (
    <Provider store={store}>
      <div>
        <Router history={browserHistory}>
          {routes(store)}
        </Router>
      </div>
    </Provider>
  );
};

export default App;
