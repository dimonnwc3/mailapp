import React, {Component, PropTypes} from 'react';
import {Link, withRouter} from 'react-router';
import {logOut} from '../../AC/auth';
import {connect} from 'react-redux';

class Header extends Component {
  static propTypes = {
    user: PropTypes.object,
    isLogged: PropTypes.bool.isRequired
  };

  render() {

    const {user, isLogged, router} = this.props;
    const {isActive} = router;

    const auth = isLogged && user ?
      <li>
        <a href="#" onClick={this.handleLogout}>Logout ({user.email})</a>
      </li> :
      <li className={isActive('/auth') ? 'active' : ''}>
        <Link to='/auth'>Login</Link>
      </li>;

    const nav = isLogged && user ?
      <ul className='nav navbar-nav'>
        <li className={isActive('/mail') ? 'active' : ''}>
          <Link to='/mail'>Mail</Link>
        </li>
        <li className={isActive('/contacts') ? 'active' : ''}>
          <Link to='/contacts'>Contacts</Link>
        </li>
      </ul> : null;

    return (
      <div>
        <nav className='navbar navbar-default navbar-fixed-top'>
          <div className='container-fluid'>
            <div className='navbar-header'>
              <Link className='navbar-brand' to='/'>Mailapp</Link>
            </div>

            <div className='collapse navbar-collapse'>
              {nav}
              <ul className='nav navbar-nav navbar-right'>
                {auth}
              </ul>
            </div>

          </div>
        </nav>
      </div>
    );
  }

  handleLogout = (e) => {
    e.preventDefault();
    this.props.logOut();
  }

}

export default connect(({user, auth}) => {
  return {
    user: user.entities[auth.myId],
    isLogged: auth.isLogged
  };
}, {logOut})(withRouter(Header));
