import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {getFolders} from '../../AC/folders';
import Loader from '../Common/Loader';
import Folder from './Folder';
import Compose from './Compose';
import {Overlay} from 'react-bootstrap';
import {hasClass} from '../../helpers/utils';

class FolderList extends Component {
  static propTypes = {
    loading    : PropTypes.bool.isRequired,
    loaded     : PropTypes.bool.isRequired,
    folders    : PropTypes.object.isRequired,
    getFolders : PropTypes.func.isRequired
  };

  state = {
    compose: false
  }

  componentDidMount() {
    const {getFolders, loading, loaded} = this.props;
    if (!loaded && !loading) getFolders();
  }

  handleComposeClick = (e) => {
    const callerBool =
      e ? !hasClass(e.target, 'glyphicon-remove') : false;
    this.setState({compose: callerBool});
  }

  render() {
    const overlay =
      <Overlay
        show={this.state.compose}>
        <Compose onClose={this.handleComposeClick}/>
      </Overlay>;
    const {loading, loaded, folders} = this.props;

    if(!loaded && loading) return <Loader />;

    const foldersBody = folders.map(f =>
      <li key={f._id} className="list-group-item">
        {(() => {
          if ( f.name === 'sent' || f.name === 'trash' || !f.unread ) {
            return;
          }
          return <span className="badge">{f.unread}</span>;
        })()}
        <Folder name={f.name} />
      </li>
    );

    return (
      <div className="col-xs-12">
        <button
          type="button"
          ref="target"
          onClick={this.handleComposeClick}
          className="btn btn-primary btn-compose">COMPOSE</button>
        <ul className="list-group">
          {foldersBody}
        </ul>
        {overlay}
      </div>
    );
  }


}

export default connect(({folders}) => {
  return {
    loading: folders.get('loading'),
    loaded : folders.get('loaded'),
    folders: folders.get('entities').valueSeq()
  };
}, {getFolders})(FolderList);
