import React, {Component} from 'react';
import {connect} from 'react-redux';
import {updateMail} from '../../AC/mail';
import {Popover, OverlayTrigger} from 'react-bootstrap';

class MailItem extends Component {

  componentDidMount() {
    const { item, updateMail, item: {_id, folder} } = this.props;
    if (!item.read) {
      const updatedItem = item.set('read', true);
      updateMail(_id, updatedItem, folder);
    }
  }

  handleUpdatingClick = () => {
      const { updateMail, item, item: {_id} } = this.props;
      const updatedItem = item.set('starred', !item.get('starred'));
      updateMail(_id, updatedItem);
  }

  render() {
    const {
      content, date, from, subject, to, starred
    } = this.props.item;
    const img = 'http://www.qatarliving.com/sites/all/themes/qatarliving_v3/images/avatar.jpeg';
    const classStarred = starred ? 'glyphicon-star' : 'glyphicon-star-empty';
    const popover = (
      <Popover id="popover-positioned-right" title="Info">
        <p>
          <span className="popover-span">from:</span>
          <span><strong>{from}</strong></span>
        </p>
        <p>
          <span className="popover-span">to:</span>
          <span>{to}</span>
        </p>
        <p>
          <span className="popover-span">date:</span>
          <span>{date}</span>
        </p>
        <p>
          <span className="popover-span">subject:</span>
          <span>{subject}</span>
        </p>
      </Popover>
    );
    return(
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-10 col-lg-8">
          <div className="well">
              <h2>{subject}</h2>
              <div className="media">
                <div className="media-left">
                  <img className="media-object" src={img} />
                </div>
                <div className="media-body">
                  <h5 className="media-heading">{from}</h5>
                  <span>
                    to {to}
                    <OverlayTrigger
                      trigger="click"
                      placement="right"
                      overlay={popover}>
                      <span className="popover-trigger
                        glyphicon glyphicon-info-sign" />
                    </OverlayTrigger>
                  </span>
                </div>
                <div className="media-right">
                  <span>{date}</span>
                  <span
                    onClick={this.handleUpdatingClick}
                    className={`star glyphicon ${classStarred}`} />
                  <span className="glyphicon glyphicon-share-alt" />
                </div>
              </div>
              <p>{content}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({mail}, props) => {
  const location = props.location.pathname.split('/');
  const id = location[location.length-1];
  const item = mail.getIn(['entities', id]);
  return {item};
}, {updateMail})(MailItem);
