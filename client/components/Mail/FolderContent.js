import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMailIn} from '../../AC/mail';
import Loader from '../Common/Loader';
import FolderMailItem from './FolderMailItem';

class FolderContent extends Component {

  componentDidMount() {
    const { folder, id, loading, loaded, getMailIn } = this.props;
    if ( !loading && !loaded && id ) getMailIn(folder, id);
  }

  componentWillReceiveProps(nextProps) {
    const { folder, id, loading, loaded, getMailIn } = nextProps;
    if ( !loading && !loaded && id ) getMailIn(folder, id);
  }

  render() {
    const { loading, mail, folder } = this.props;
    if ( loading ) return <Loader />;
    if (!mail.count()) return (
      <p>Nothing here yet. Or finally.</p>
    );
    const mailListBody =
      mail.sort((a, b) => new Date(b.date) - new Date(a.date))
          .map(m => <FolderMailItem key={m._id} data={m} folder={folder}/> );
    return (
      <div className="row">
        <div className="col-sm-12">
          <table className="table table-hover">
            <colgroup>
              <col className="col-checkbox"/>
              <col className="col-checkbox"/>
              <col className="col-from"/>
              <col className="col-body"/>
              <col className="col-date"/>
            </colgroup>
            <tbody>
              {mailListBody}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default connect(
  ({mail, folders}, {params: {folder}}) => {
    const folderInState = folders.get('entities').valueSeq()
      .filter(f => f.name === folder).first();
    const id = folderInState ? folderInState._id : null;
    return {
      loading: mail.get('loading'),
      loaded: folders.getIn(['entities', id, 'loaded']),
      mail: mail.get('entities').valueSeq()
        .filter(m => m.folder === id),
      folder, id
    };
}, {getMailIn})(FolderContent);
