import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

class Folder extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired
  }

  render() {
    const {name} = this.props;
    const capName = name[0].toUpperCase() + name.substr(1);
    return (
      <Link to={`/mail/${name}`}>
        {capName}
      </Link>
    );
  }
}

export default Folder;
