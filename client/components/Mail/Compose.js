import React, {Component} from 'react';
import {MailItem} from '../../reducer/mail';
import {connect} from 'react-redux';
import {sendMail} from '../../AC/mail';

class Compose extends Component {

  state = {
    message: new MailItem()
  }

  componentWillReceiveProps(next) {
    if (this.props.creating && !next.creating) {
      this.props.onClose();
    }
  }

  handleChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      message: this.state.message.set(name, value)
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const {creating} = this.props;
    if (creating) return;
    const {myEmail, sentId, sendMail} = this.props;
    const {message} = this.state;
    const prepared = message
      .set('from', myEmail)
      .set('folder', sentId)
      .set('read', true);
    sendMail(prepared);
  }

  render() {
    const {creating} = this.props;
    return (
      <div className="well col-xs-9 col-sm-6 col-lg-5" id="compose">
        <div>
          <h4 id="compose-heading">New email
            <span
              className="glyphicon glyphicon-remove"
              onClick={this.props.onClose}/>
          </h4>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="compose-to">To</label>
            <input
              onChange={this.handleChange}
              type="email"
              name="to"
              id="compose-to"
              className="form-control"
              disabled={creating}/>
          </div>
          <div className="form-group">
            <label htmlFor="compose-subject">Subject</label>
            <input
              onChange={this.handleChange}
              type="text"
              name="subject"
              id="compose-subject"
              className="form-control"
              disabled={creating}/>
          </div>
          <div className="form-group">
            <label htmlFor="compose-body">Text</label>
            <textarea
              onChange={this.handleChange}
              name="content"
              id="compose-body"
              className="form-control"
              rows="5"
              disabled={creating}/>
          </div>
          <button
            type="submit"
            className="btn btn-default"
            disabled={creating}>Send
          </button>
        </form>
      </div>
    );
  }
}

export default connect(({user, auth, folders, mail}) => ({
  creating: mail.get('creating'),
  myEmail: user.entities[auth.myId].email,
  sentId: folders.get('entities')
    .filter(f => f.name === 'sent').first().get('_id')
}), {sendMail})(Compose);
