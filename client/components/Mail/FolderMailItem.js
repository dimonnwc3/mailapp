import React, {Component, PropTypes} from 'react';
import './styles.css';
import {updateMail} from '../../AC/mail';
import {connect} from 'react-redux';
import {hasClass} from '../../helpers/utils';

class FolderMailItem extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  handleClick = () => {
    const { _id } = this.props.data;
    const { folder } = this.props;
    this.context.router.push(`/mail/${folder}/${_id}`);
  }

  handleUpdatingClick = (e) => {
    e.stopPropagation();
    if ( hasClass(e.target, 'star') ) {
      const { updateMail, data, data: {_id} } = this.props;
      const updatedItem = data.set('starred', !data.get('starred'));
      updateMail(_id, updatedItem);
    }
  }

  render() {

    const {
      _id, from, to, subject, content, date, read, starred
    } = this.props.data;
    const { folder } = this.props;
    const classBold = !read ? 'table-row-bold' : '';
    const classStarred = starred ? 'glyphicon-star' : 'glyphicon-star-empty';

    return (
      <tr key={_id} className={classBold} onClick={this.handleClick}>
        <td className="td-checkbox" onClick={this.handleUpdatingClick}>
          <input type="checkbox"/>
        </td>
        <td className="td-checkbox" onClick={this.handleUpdatingClick}>
          <span className={`star glyphicon ${classStarred}`} />
        </td>
        <td className="td-from">
          <span>{folder === 'sent' ? `To: ${to}` : from}</span>
        </td>
        <td className="td-body">
          <span>{subject} </span>
           —
          <span> {content}</span>
        </td>
        <td><span>{date}</span></td>
      </tr>
    );
  }
}

export default connect(null, {updateMail})(FolderMailItem);
