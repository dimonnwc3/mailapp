import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {logIn} from '../../AC/auth';

class Login extends Component {
  static propTypes = {
    logging: PropTypes.bool.isRequired
  };

  state = {
    email: 'admin@google.com',
    password: 'admin'
  }

  render() {

    const {email, password} = this.state;
    const {logging} = this.props;

    const legend = <legend>Login</legend>;

    const emailBody =
      <div className="form-group">

        <label htmlFor="emailLogin"
               className='control-label'>
          Email
        </label>

        <input type="text"
               className='form-control'
               name='email'
               id='emailLogin'
               value={email}
               onChange={this.handleFieldChange}/>

      </div>;

    const passwordBody =
      <div className="form-group">

        <label htmlFor="passwordLogin"
               className='control-label'>
          Email
        </label>

        <input type="password"
               className='form-control'
               name='password'
               id='passwordLogin'
               value={password}
               onChange={this.handleFieldChange}/>

      </div>;

    return (
      <div>
        <form name='logIn' onSubmit={this.handleSubmit}>
          <div className="well">
            <fieldset>
              {legend}
              {emailBody}
              {passwordBody}
              <button className='btn btn-primary'
                      disabled={logging}
                      type='submit'>Login
              </button>
            </fieldset>
          </div>
        </form>
      </div>
    );
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.logIn(this.state);
  }

  handleFieldChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      [name]: value
    });
  }

}

export default connect(({auth}) => {
  return {
    logging: auth.logging
  };
}, {logIn})(Login);
