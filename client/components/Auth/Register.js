import React, {Component} from 'react';
import {connect} from 'react-redux';
import {logIn} from '../../AC/auth';

class Register extends Component {
  static propTypes = {};

  state = {
    email   : '',
    password: ''
  }

  render() {

    const {email, password} = this.state;

    const legend = <legend>Register</legend>;

    const emailBody =
            <div className="form-group">

              <label htmlFor="emailRegister"
                     className='control-label'>
                Email
              </label>

              <input type="text"
                     className='form-control'
                     name='email'
                     id='emailRegister'
                     value={email}
                     onChange={this.handleFieldChange}/>

            </div>;

    const passwordBody =
            <div className="form-group">

              <label htmlFor="passwordRegister"
                     className='control-label'>
                Email
              </label>

              <input type="password"
                     className='form-control'
                     name='password'
                     id='passwordRegister'
                     value={password}
                     onChange={this.handleFieldChange}/>

            </div>;

    return (
      <div>
        <form name='logIn' onSubmit={this.handleSubmit}>
          <div className="well">
            <fieldset>
              {legend}
              {emailBody}
              {passwordBody}
            </fieldset>
          </div>
        </form>
      </div>
    );
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.logIn(this.state);
  }

  handleFieldChange = (e) => {
    const {name, value} = e.target;
    this.setState({
      [name]: value
    });
  }

}

export default connect(null, {logIn})(Register);
