import {createStore, applyMiddleware, compose} from 'redux';
import reducer from '../reducer';

import thunk from '../middlewares/01-thunk';
import api from '../middlewares/02-api';
import normalize from '../middlewares/03-normalize';
import notification from '../middlewares/04-notification';
import logger from '../middlewares/05-logger';

const enhancer = compose(
  applyMiddleware(thunk, api, normalize, notification, logger),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

export default function(state = {}) {
  return createStore(reducer, state, enhancer);
}
