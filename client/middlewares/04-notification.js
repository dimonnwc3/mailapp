import {addNotification} from '../AC/notifications';

import {START, SUCCESS, FAIL} from '../constants';

export default ({dispatch}) => next => action => {

  const {type, notification, ...rest} = action;

  if (!notification) return next(action);

  switch (true) {

    case Boolean(type.includes(SUCCESS) && notification.success):
      dispatch(addNotification({
        type: notification.success.type || 'success',
        message: notification.success.message ||
        'Action performed successfully',
        dismissAfter: notification.success.dismissAfter
      }));
      break;

    case Boolean(type.includes(START) && notification.start):
      dispatch(addNotification({
        type: notification.success.type || 'success',
        message: notification.success.message || 'Action started successfully',
        dismissAfter: notification.success.dismissAfter
      }));
      break;

    case Boolean(type.includes(FAIL) && notification.error):
      dispatch(addNotification({
        type: notification.error && notification.error.type || 'error',
        message: notification.error && notification.error.message ||
        'An error occurred',
        dismissAfter: notification.error && notification.error.dismissAfter
      }));
      break;

    case Boolean(notification && notification.type || notification.message):
      dispatch(addNotification({
        type: notification.type || 'success',
        message: notification.message || 'Action performed successfully',
        dismissAfter: notification.error.dismissAfter
      }));
      break;

    case type.includes(FAIL):
      dispatch(addNotification({
        type: 'error',
        message: 'An error occurred'
      }));
      break;

  }

  next({type, notification, ...rest});

};
