import axios from 'axios';

import {
  START,
  SUCCESS,
  FAIL,
  GET,
  UPDATE,
  DELETE,
  CREATE,
  SESSION_EXPIRED
} from '../constants';

export default () => next => action => {

  const {api, type, ...rest} = action;

  if (!api) return next(action);

  next({type: type + START, ...rest});

  let promise;

  switch (true) {

    case type.includes(GET):
      promise = axios.get(api);
      break;

    case type.includes(CREATE): {
      const {data} = action.payload;
      promise = axios.post(api, data.toJS && data.toJS() || data);
      break;
    }

    case type.includes(UPDATE): {
      const {data} = action.payload;
      promise = axios.patch(api, data.toJS && data.toJS() || data);
      break;
    }

    case type.includes(DELETE):
      promise = axios.delete(api);
      break;

  }

  promise
    .then(res => next({type: type + SUCCESS, res, ...rest}))
    .catch(err => {
      if (err.status === 403) {
        next({type: SESSION_EXPIRED});
      }
      next({type: type + FAIL, err: err.response, ...rest});
    });

};
