import {arrayOf, normalize} from 'normalizr';

export default () => next => action => {

  const {schema, res, ...rest} = action;

  if (!schema || !res) return next(action);

  const normalized = {
    ...res,
    data: normalize(res.data.entities, arrayOf(schema))
  };

  next({res: normalized, ...rest});

};