import React from 'react';
import authRequired from './decorators/authRequired';
import noAuthRequired from './decorators/noAuthRequired';

import {
  Route, IndexRoute, IndexRedirect, Redirect
} from 'react-router';

import Root from './RouteHandlers/Root';
import Auth from './RouteHandlers/Auth';

import Contacts from './RouteHandlers/Contacts';
import ContactsIndex from './components/Contacts/Index';
import ContactsEdit from './components/Contacts/Edit';
import ContactsCreate from './components/Contacts/Create';
import ContactsPreview from './components/Contacts/Preview/ContactsPreview';

import Mail from './RouteHandlers/Mail';
import FolderContent from './components/Mail/FolderContent';
import MailItem from './components/Mail/MailItem';

export default function() {
  return (
    <Route path='/' component={Root}>
      <IndexRedirect to='mail'/>
      <Redirect from='mail' to='mail/inbox'/>

      <Route path='contacts' component={authRequired(Contacts)}>
        <IndexRoute component={ContactsIndex}/>
        <Route path='create' component={ContactsCreate}/>
        <Route path='edit/:id' component={ContactsEdit}/>
        <Route path=':id' component={ContactsPreview}/>
      </Route>

      <Route path='mail' component={authRequired(Mail)}>
        <Route path='compose'/>
        <Route path=':folder' component={FolderContent}/>
        <Route path=':folder/:mailitem' component={MailItem}/>
      </Route>

      <Route path='auth' component={noAuthRequired(Auth)}/>
    </Route>
  );
}
