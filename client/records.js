import {Record} from 'immutable';

export const Contact = new Record({
  _id           : '',
  firstName     : '',
  lastName      : '',
  dob           : '',
  avatarUrl     : 'https://www.gravatar.com/avatar/00000000000000000000000000000000?',
  comment       : '',
  user          : '',
  addresses     : [],
  communications: [],
  loading       : false,
  loaded        : false,
  updating      : false,
  deleting      : false,
  isNew         : false
});

export const Address = new Record({
  _id    : '',
  type   : 'Postal',
  comment: '',
  country: '',
  zip    : '',
  city   : '',
  street : '',
  houseNo: ''
});

export const Communication = new Record({
  _id    : '',
  type   : 'Phone',
  content: '',
  comment: ''
});