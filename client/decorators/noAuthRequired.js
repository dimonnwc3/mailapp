import React, {PropTypes} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

export default (Component) => {

  class NoAuthRequired extends React.Component {

    static propTypes = {
      isLogged: PropTypes.bool.isRequired
    };

    componentWillMount() {
      this.checkAuth(this.props.isLogged);
    }

    componentWillReceiveProps(nextProps) {
      this.checkAuth(nextProps.isLogged);
    }

    checkAuth = (isLogged) => {
      if (isLogged) {
        const {router, location} = this.props;
        const {redirectTo} = location.query;
        router.push(redirectTo || '/');
      }
    }

    render() {

      const {isLogged, ...rest} = this.props;

      if (isLogged) return null;

      return <Component {...rest}/>;

    }

  }

  return connect(({auth}) => {
    return {isLogged: auth.isLogged};
  })(withRouter(NoAuthRequired));

};
