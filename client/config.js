import axios from 'axios';

axios.defaults.baseURL = '/api';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.patch['Accept'] = 'application/json';
axios.defaults.headers.patch['Content-Type'] = 'application/json';