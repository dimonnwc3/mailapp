import {ME, GET} from '../constants';

export function getMe() {
  return {
    type: ME + GET,
    api: '/users/me'
  };
}
