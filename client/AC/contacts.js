import {contact} from '../schemas';

import {
  CONTACTS,
  CONTACT,
  GET,
  UPDATE,
  CREATE,
  DELETE,
  FILTER
} from '../constants';

export function getContacts() {
  return {
    type: CONTACTS + GET,
    api : '/contacts',
    schema: contact
  };
}

export function getContact(id) {
  return {
    type   : CONTACT + GET,
    payload: {id},
    api    : `/contacts/${id}`,
    schema: contact
  };
}

export function createContact(data) {
  return {
    type   : CONTACT + CREATE,
    payload: {data},
    api    : `/contacts`,
    schema: contact
  };
}

export function updateContact(id, data) {
  return {
    type   : CONTACT + UPDATE,
    payload: {id, data},
    api    : `/contacts/${id}`,
    schema: contact
  };
}

export function deleteContact(id) {
  return {
    type   : CONTACT + DELETE,
    payload: {id},
    api    : `/contacts/${id}`
  };
}

export function filterContacts(term) {
  return {
    type: CONTACTS + FILTER,
    payload: {term}
  };
}
