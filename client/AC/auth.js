import axios from 'axios';

import {LOGIN, LOGOUT, START, SUCCESS, FAIL} from '../constants';

export function logIn(credentials) {
  return function(dispatch) {

    dispatch({type: LOGIN + START});

    axios.post('/auth/login', credentials)
      .then(res => dispatch({type: LOGIN + SUCCESS, res: res}))
      .catch(err => dispatch({type: LOGIN + FAIL, err: err.response}));

  };
}

export function logOut() {
  return function(dispatch) {

    dispatch({type: LOGOUT + START});

    axios.get('/auth/logout')
      .then(res => dispatch({type: LOGOUT + SUCCESS, res: res}))
      .catch(err => dispatch({type: LOGOUT + FAIL, err: err.response}));

  };
}
