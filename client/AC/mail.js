import {MAIL, GET, UPDATE, CREATE, MAIL_SENT_MSG} from '../constants';

export function getMailIn(folder, id) {
  return {
    type: MAIL + GET,
    payload: {folder, id},
    api: `/mails/${folder}`
  };
}

export function updateMail(id, data, updateCounter = false) {
  return {
    type: MAIL + UPDATE,
    payload: {id, data},
    updateCounter,
    api: `/mails/${id}`
  };
}

export function sendMail(data) {
  return {
    type: MAIL + CREATE,
    payload: {data},
    api: 'mails',
    notification: {
      success: {message: MAIL_SENT_MSG}
    }
  };
}
