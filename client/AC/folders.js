import { FOLDERS, GET } from '../constants';

export function getFolders() {
  return {
    type: FOLDERS + GET,
    api: '/folders'
  };
}
