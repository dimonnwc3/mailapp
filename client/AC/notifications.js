import {NOTIFICATION, ADD, DELETE} from '../constants';

export function addNotification(notification) {
  if (!notification.key) {
    notification.key = Date.now();
  }
  return {
    type: NOTIFICATION + ADD,
    payload: {notification}
  };
}

export function deleteNotification(notification) {
  return {
    type: NOTIFICATION + DELETE,
    payload: {notification}
  };
}
