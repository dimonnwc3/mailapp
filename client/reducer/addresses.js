import {CONTACT, SUCCESS, GET, CREATE, UPDATE} from '../constants';
import {merge} from 'lodash';

const defaultState = {
  entities: {}
};

export default (state = defaultState, action) => {
  const {type, res} = action;

  switch (type) {

    case CONTACT + GET + SUCCESS:
    case CONTACT + CREATE + SUCCESS:
    case CONTACT + UPDATE + SUCCESS:
      return {
        ...state,
        entities: merge(state.entities, res.data.entities.addresses)
      };

  }

  return state;

};