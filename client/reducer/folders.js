import {GET, UPDATE, START, SUCCESS, FOLDERS, MAIL} from '../constants';
import {Map, OrderedMap, Record} from 'immutable';
import {arrayToMap} from '../helpers/utils';

const Folder = new Record({
  _id: '',
  updatedAt: '',
  createdAt: '',
  name: '',
  unread: null,
  // temporary entry
  user: '',
  loaded: false
});

const defaultState = new Map({
  loading: false,
  loaded: false,
  entities: new OrderedMap({})
});

export default (state = defaultState, action) => {
  const {type, res, payload} = action;

  switch (type) {
    case FOLDERS + GET + START:
      return state.set('loading', true);
    case FOLDERS + GET + SUCCESS:
      return state.updateIn(['entities'],
        e => e.merge(arrayToMap(res.data.entities, Folder))
          // temporary until after auth is done
          .filter(f => f.user === '7c10223c510628f39d16c4eb'))
        .set('loading', false)
        .set('loaded', true);
    case MAIL + UPDATE + SUCCESS:
      return action.updateCounter ?
        state.setIn(['entities', action.updateCounter, 'unread'],
        state.getIn(['entities', action.updateCounter, 'unread']) - 1) : state;
    case MAIL + GET + SUCCESS:
      return state.setIn(['entities', payload.id, 'loaded'], true);
  }

  return state;
};
