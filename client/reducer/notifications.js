import {NOTIFICATION, ADD, DELETE} from '../constants';

const defaultState = {
  entities: []
};

export default (state = defaultState, action) => {
  const {type, payload} = action;

  switch (type) {
    case NOTIFICATION + ADD: {
      const {notification} = payload;
      return {
        ...state,
        entities: state.entities.concat([notification])
      };
    }

    case NOTIFICATION + DELETE: {
      const {notification} = payload;
      return {
        ...state,
        entities: state.entities.filter(i => i.key !== notification.key)
      };
    }

  }

  return state;
};
