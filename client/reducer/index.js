import {combineReducers} from 'redux';

import {reducer as formReducer} from 'redux-form';

import contacts from './contacts';
import folders from './folders';
import mail from './mail';
import user from './users';
import auth from './auth';
import addresses from './addresses';
import communications from './communications';
import notifications from './notifications';

export default combineReducers({
  form: formReducer,
  communications,
  addresses,
  contacts,
  folders,
  mail,
  user,
  auth,
  notifications
});
