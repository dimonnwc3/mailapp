import {MAIL, GET, UPDATE, CREATE, START, SUCCESS} from '../constants';
import {Map, OrderedMap, Record} from 'immutable';
import {arrayToMap} from '../helpers/utils';

export const MailItem = new Record({
  _id: '',
  folder: '',
  updatedAt: '',
  createdAt: '',
  // temporary
  user: '',
  from: '',
  to: '',
  ip: '',
  subject: '',
  content: '',
  date: '',
  read: false,
  starred: false,
  updating: false
});

const defaultState = new Map({
  entities: new OrderedMap({}),
  loading: false,
  creating: false
});

export default (state = defaultState, action) => {
  const {type, res, payload} = action;

  switch (type) {
    case MAIL + GET + START:
      return state.set('loading', true);
    case MAIL + GET + SUCCESS:
      return state
        .updateIn(['entities'], e =>
          e.merge(arrayToMap(res.data.entities, MailItem)))
        .set('loading', false);
    case MAIL + UPDATE + START:
      return state.setIn(['entities', payload.id, 'updating'], true);
    case MAIL + UPDATE + SUCCESS:
      return state
        .setIn(['entities', payload.id], new MailItem(res.data.entities[0]))
        .setIn(['entities', payload.id, 'updating'], false);
    case MAIL + CREATE + START:
      return state.set('creating', true);
    case MAIL + CREATE + SUCCESS:
      return state
        .setIn(['entities', res.data.entities[0]._id],
          new MailItem(res.data.entities[0]))
        .set('creating', false);
  }

  return state;
};
