import {
  ME, START, SUCCESS, FAIL, GET, LOGOUT, LOGIN, SESSION_EXPIRED
} from '../constants';

const defaultState = {
  isLogged: false,
  myId: '',
  loading: false,
  loaded: false,
  logging: false
};

export default (state = defaultState, action) => {
  const {type, res} = action;

  switch (type) {

    case ME + GET + START:
      return {...state, loading: true};

    case ME + GET + SUCCESS:
      return {
        ...state,
        myId: res.data.entities[0]._id,
        isLogged: true,
        loaded: true,
        loading: false
      };

    case ME + GET + FAIL:
      return {...state, loading: false, loaded: true};

    case LOGIN + START:
      return {...state, logging: true};

    case LOGIN + SUCCESS:
      return {...state, logging: false, isLogged: true};

    case LOGIN + FAIL:
      return {...state, logging: false, isLogged: false};

    case LOGOUT + START:
      return {...state, logging: true};

    case LOGOUT + SUCCESS:
    case SESSION_EXPIRED:
      return {
        ...state,
        myId: '',
        isLogged: false,
        logging: false
      };

  }

  return state;

};
