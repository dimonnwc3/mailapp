import {
  CONTACTS,
  CONTACT,
  SUCCESS,
  FAIL,
  START,
  GET,
  UPDATE,
  CREATE,
  DELETE,
  FILTER
} from '../constants';

const defaultState = {
  filter: '',
  loading: false,
  loaded: false,
  creating: false,
  entities: {}
};

import {merge, omit} from 'lodash';

export default (state = defaultState, action) => {

  const {type, payload, res} = action;

  switch (type) {

    case CONTACTS + GET + START:
      return {...state, loading: true};

    case CONTACTS + GET + SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        entities: merge(state.entities, res.data.entities.contacts)
      };

    case CONTACTS + GET + FAIL:
      return {...state, loading: false};

    case CONTACTS + FILTER:
      return {...state, filter: payload.term};

    case CONTACT + GET + START: {
      const entity = state.entities[payload.id] || {};

      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...entity, loading: true, loaded: false}
        }
      };
    }

    case CONTACT + GET + SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {
            ...res.data.entities.contacts[payload.id],
            loading: false,
            loaded: true
          }
        }
      };

    case CONTACT + GET + FAIL: {
      const entity = state.entities[payload.id] || {};

      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...entity, loading: false, loaded: false}
        }
      };
    }

    case CONTACT + CREATE + START:
      return {...state, creating: true};

    case CONTACT + CREATE + SUCCESS:
      return {
        ...state,
        creating: false,
        entities: merge(state.entities, res.data.entities.contacts)
      };

    case CONTACT + CREATE + FAIL:
      return {...state, creating: false};

    case CONTACT + UPDATE + START:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...state.entities[payload.id], updating: true}
        }
      };

    case CONTACT + UPDATE + SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {
            ...res.data.entities.contacts[payload.id],
            loaded: true,
            loading: false,
            updating: false
          }
        }
      };

    case CONTACT + UPDATE + FAIL:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...state.entities[payload.id], updating: false}
        }
      };

    case CONTACT + DELETE + START:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...state.entities[payload.id], deleting: true}
        }
      };

    case CONTACT + DELETE + SUCCESS:
      return {
        ...state,
        entities: {
          ...omit(state.entities, [payload.id])
        }
      };

    case CONTACT + DELETE + FAIL:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload.id]: {...state.entities[payload.id], deleting: false}
        }
      };

  }

  return state;
};
