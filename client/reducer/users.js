import {ME, USER, SUCCESS, GET} from '../constants';

const defaultState = {
  loading: false,
  loaded: false,
  entities: {}
};

export default (state = defaultState, action) => {
  // const {type, payload, res, err} = action;
  const {type, payload, res} = action;

  switch (type) {

    case USER + GET + SUCCESS:
    case ME + GET + SUCCESS:
      return {
        ...state,
        entities: {
          ...state.entities,
          [payload && payload.id || res.data.entities[0]._id]: {
            ...res.data.entities[0],
            loading: false,
            loaded: true
          }
        }
      };

  }

  return state;

};
